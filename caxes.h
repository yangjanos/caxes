#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sys/stat.h>
#include <ctime>

#include <vigra/stdimage.hxx>
#include <vigra/impex.hxx>
#include <vigra/labelimage.hxx>
#include <vigra/seededregiongrowing.hxx>
#include <vigra/functorexpression.hxx>
#include <vigra/tinyvector.hxx>
#include <vigra/linear_algebra.hxx>
#include <vigra/watersheds.hxx>
#include <vigra/flatmorphology.hxx>
#include <vigra/multi_array.hxx>
#define _USE_MATH_DEFINES 

#include <cgp/cgp_hdf5.hxx>
#include "cgp_structure.hxx"
#include "marray.hxx"
#include "seg.h"

std::string cAxes_version = "21.06.21 slim";

class Segment
{
    public:
    //Attributes *************************************************************
    std::vector<point>          pointer;
    int                         phase; // background=0, ice=1 
    int                         size;
    double                      xcenter;
    double                      ycenter;
    int                         child_index; // reference to the child, e.g. grain index
};

class Grains: public Segment //child object of the parent Segment
{
    public:
    //Attributes *************************************************************    
    int                         parent_index; // reference to the parent region
    vigra::TinyVector<float, 3> axis;
    float                       az;
    float                       co;
    float                       r;
};

class Boundaries
{
    public:
    //Attributes *************************************************************
    float                       length;
    std::vector<int>            grain;
    std::vector<point>          junction;
    int                         xdist;
    int                         ydist;
    double                      misorientation;
    double                      jjratio;
    double                      jjdistance;
    double                      slope;
    std::vector<point>          pointer;
    vigra::TinyVector<float, 3> crossproduct;
    float                       cross_az;
    float                       cross_co;
    float                       cross_slope;
    float                       tilt_to_boundary;
    float                       twist_to_boundary;
};

/*vpn.awi.de
typedef struct
{
    int size;
    double xcenter;
    double ycenter;
    vigra::TinyVector<float, 3> axis;
    float az;
    float co;
    float r;
}   grainStruct;*/

//global variables
int mWidth, mHeight, pixelsize, tiles_wide, tiles_high, tile_size, borderpixel;
float maximal_misorientation, rot_angle, scanned_width, scanned_height, rotate_north;
int rqmin, gqmin, minimal_grain_size;
bool eliminate_borders, bground_mask_found, vertical_cut;
std::string sourcepath, instrument, date, mineral;

vigra::BasicImage< vigra::TinyVector <float, 3> > vector_image(1,1);
//vigra::BasicImage< vigra::TinyVector <float, 3> > vector_image2(1,1);
vigra::BImage quality_image(1,1);

//grain statistics
int nr_regions, nr_grains, mean_size;
float sum_vector_norm, regelungsgrad, regelungsgrad_g, concentration_parameter, concentration_parameter_g, spherical_aperture, spherical_aperture_g, woodcock, woodcock_g,
e1, e2, e3, e1_g, e2_g, e3_g;
float eigenv_a_az[3], eigenv_g_az[3], eigenv_a_co[3], eigenv_g_co[3]; // (az_e1, az_e2, az_e3) and so on
vigra::TinyVector <float, 3> eigenvector_a[3], eigenvector_g[3];

//boundary statistics
int nr_boundaries;

//colors
vigra::RGBValue<float, 0,1,2> white (1.0,1.0,1.0);
vigra::RGBValue<float, 0,1,2> black (0.0,0.0,0.0);
vigra::RGBValue<float, 0,1,2> red (1.0,0.0,0.0);
vigra::RGBValue<float, 0,1,2> green (0.0,1.0,0.0);
vigra::RGBValue<float, 0,1,2> blue (0.0,0.0,1.0);

// Functions
bool file_exists(std::string filename)
{
    FILE* fp = NULL;
    fp = fopen( filename.c_str(), "rb" );
    if( fp != NULL )
    {
        fclose( fp );
        return true;
    }

    return false;
}

int rotate_north_vector_image(vigra::FVector3Image &image)
{
    vigra::FVector3Image rotated_image(mWidth, mHeight);
    float rotation_angle = rotate_north*M_PI/180;
    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
	{
	  rotated_image(x,y)[0] = cos(rotation_angle)*image(x,y)[0] + sin(rotation_angle)*image(x,y)[1];
	  rotated_image(x,y)[1] = -sin(rotation_angle)*image(x,y)[0] + cos(rotation_angle)*image(x,y)[1];
	  rotated_image(x,y)[2] = image(x,y)[2];
	}
    image = rotated_image;
    
    return 0;
}

int median_filter(vigra::FImage &image)
{
    std::vector<float> range;
    vigra::FImage src(mWidth, mHeight);
//    src = image;
    int xx,yy;
    for (int ytile = 0; ytile < tiles_high; ytile++)
    for (int xtile = 0; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
            xx=x + tile_size*xtile;
            yy=y + tile_size*ytile;
            
            range.resize(1);

            range[0] = image(xx,yy);
            if (x-1 >= 0)
            if (quality_image(xx-1,yy) == 255)
              range.push_back (image(xx-1,yy));
            if (x+1 < tile_size)
            if (quality_image(xx+1,yy) == 255)
              range.push_back (image(xx+1,yy));
            if (y-1 >= 0)
            if (quality_image(xx,yy-1) == 255)
              range.push_back (image(xx,yy-1));
            if (y+1 < tile_size)
            if (quality_image(xx,yy+1) == 255)
              range.push_back (image(xx,yy+1));
            
            if (x-1 >= 0 && y-1 >= 0)
            if (quality_image(xx-1,yy-1) == 255)
              range.push_back (image(xx-1,yy-1));
            if (x+1 < tile_size && y-1 >= 0)
            if (quality_image(xx+1,yy-1) == 255)
              range.push_back (image(xx+1,yy-1));
            if (x-1 >= 0 && y+1 < tile_size)
            if (quality_image(xx-1,yy+1) == 255)
              range.push_back (image(xx-1,yy+1));
            if (x+1 < tile_size && y+1 < tile_size)
            if (quality_image(xx+1,yy+1) == 255)
              range.push_back (image(xx+1,yy+1));
            
            sort (range.begin(), range.end());
            
            if (range.size()>0)
            src(xx,yy) = range[floor(range.size()/2)];
        }
    image = src;
    return 0;
}

int import_data_g60AWI()
{
    std::string filepath_info = sourcepath+"/info.txt";
    std::string filepath_binary = sourcepath+"/data.cis";
    
    std::string info[18];
  
    std::ifstream infofile (filepath_info.c_str());
    if (infofile.is_open())
    {
        for (int line=0; line<=17; line++)
        {
            getline (infofile, info[line]);
        }
        infofile.close();
    }
    else
    {
        //std::cout << "Unable to open " << filepath_info << std::endl;
        return 1;
    }
    
    instrument.assign(info[1].begin()+13, info[1].end()-1);
    if (instrument != "G6008 AWI")
	return 1;
    
    date.assign(info[6].begin()+7, info[6].end());
    mineral.assign(info[7].begin()+9, info[7].end());
    std::istringstream(std::string (info[8].begin()+14, info[8].end()-7)) >> mWidth;
    std::istringstream(std::string (info[9].begin()+15, info[9].end()-7)) >> mHeight;
    std::istringstream(std::string (info[10].begin()+12, info[10].end()-9)) >> pixelsize; //micrometer square
    std::istringstream(std::string (info[12].begin()+13, info[12].end())) >> tiles_wide;
    std::istringstream(std::string (info[13].begin()+13, info[13].end())) >> tiles_high;
    std::istringstream(std::string (info[14].begin()+16, info[14].end()-2)) >> scanned_width;
    std::istringstream(std::string (info[15].begin()+17, info[15].end()-2)) >> scanned_height;
    
    tile_size = mWidth/tiles_wide;
    
    
    
    

    //Load and convert binary file
    typedef struct
    {
        short az;   //azimuth in nintieths of degree (polar angle convention, not geo)
        short co;   //colat in hundredths of degree
        char ao[8]; //azimuth offsets for oblique axes (log form)
        short nm;   //retardation in nanometers
        char rq;    //retardation quality 100 - (blue+red phase error in degree)
        char gq;    //geometric quality 100=perfect 0=not usable
    }   dataStruct;

    dataStruct * mData = new dataStruct[mWidth * mHeight];
    FILE *fp = fopen(filepath_binary.c_str(), "rb");
    if (fp == NULL)
    {
        std::cout<<"Not able to open "<<filepath_binary<<std::endl;
        return 1;
    }
    int count = fread(mData, sizeof(dataStruct), mWidth * mHeight, fp);

    if(count!=mWidth * mHeight)
    {
        std::cout<<"File "<<filepath_binary<<" haven't been read correctly!"<<std::endl;
        return -1;
    }
    else std::cout<<"Reading file "<<filepath_binary<<"."<<std::endl;
    std::cout << "instrument G60: " << instrument << std::endl;
    fclose(fp);

    float * converted_ao = new float[mWidth * mHeight];

    vigra::FImage az_image(mWidth, mHeight);
    vigra::FImage co_image(mWidth, mHeight);
    quality_image.resize(mWidth, mHeight);
    vigra::TinyVector<float, 3> tiny_vector;
    vector_image.resize(mWidth, mHeight);
//    vector_image2.resize(mWidth, mHeight);
    /*

    vigra::FImage nm_image(mWidth, mHeight);
    vigra::FImage rq_image(mWidth, mHeight);
    vigra::FImage gq_image(mWidth, mHeight);
    vigra::FImage quality_image(mWidth, mHeight);
    */
    
    std::cout<<"Start conversion... ";

    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {
            int z=y*mWidth+x;

            //convert pixel data elements
            az_image(x,y)=mData[z].az/90.0;
            co_image(x,y)=mData[z].co/100.0;

            float v;
            float b;
            b=atof(mData[z].ao);
            if (b<=127)
            {
                v = b / 127.0f;
                converted_ao[z] = 10.0f * pow(10,v) - 10.0f;
            }
            else
            {
                v = (255.0f - b) / 127.0f;
                converted_ao[z] = -(10.0f * pow(10,v) - 10.0f);
            }

            //Write quality image
            if(mData[z].gq<gqmin || mData[z].rq<rqmin)
            quality_image(x,y)=0;
                else quality_image(x,y)=255;
        }
    
/*
    if (vertical_cut) //vertical thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[2]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //z
            tiny_vector[1]=cos(co_image(x,y)*M_PI/180);                    //y
            vector_image2(x, y) = tiny_vector;    
        }
    else  //horizontal thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[1]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //y
            tiny_vector[2]=-cos(co_image(x,y)*M_PI/180);                    //z
            vector_image2(x, y) = tiny_vector;    
        }
*/ 
    //apply median filter
//    median_filter(az_image);
    median_filter(co_image);
    if (vertical_cut) //vertical thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[2]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //z
            tiny_vector[1]=cos(co_image(x,y)*M_PI/180);                    //y
            vector_image(x, y) = tiny_vector;    
        }
    else  //horizontal thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[1]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //y
            tiny_vector[2]=-cos(co_image(x,y)*M_PI/180);                    //z
            vector_image(x, y) = tiny_vector;    
        }
        
    std::cout << "done" << std::endl;
        
    delete converted_ao;
    delete mData;
    
    return 0;
}

int import_data_g50AWI()
{
    std::string filepath_info = sourcepath+"/info.txt";
    std::string filepath_binary = sourcepath+"/data.cis";
    
    std::string info[14];
  
    std::ifstream infofile (filepath_info.c_str());
    if (infofile.is_open())
    {
        for (int line=0; line<=12; line++)
        {
            getline (infofile, info[line]);
        }
        infofile.close();
    }
    else
    {
        //std::cout << "Unable to open " << filepath_info << std::endl;
        return 1;
    }
    
    instrument.assign(info[1].begin()+13, info[1].end()-1);
    if (instrument != "G500701 Bremerhaven")
	return 1;
    
    date.assign(info[3].begin()+7, info[3].end());
    mineral.assign(info[4].begin()+9, info[4].end());
    std::istringstream(std::string (info[5].begin()+14, info[5].end()-7)) >> mWidth;
    std::istringstream(std::string (info[6].begin()+15, info[6].end()-7)) >> mHeight;
    std::istringstream(std::string (info[7].begin()+12, info[7].end()-9)) >> pixelsize; //micrometer square
    std::istringstream(std::string (info[9].begin()+13, info[9].end())) >> tiles_wide;
    std::istringstream(std::string (info[10].begin()+13, info[10].end())) >> tiles_high;
    std::istringstream(std::string (info[11].begin()+16, info[11].end()-2)) >> scanned_width;
    std::istringstream(std::string (info[12].begin()+17, info[12].end()-2)) >> scanned_height;
    
    tile_size = mWidth/tiles_wide;
    
    

    //Load and convert binary file
    typedef struct
    {
        short az;   //azimuth in nintieths of degree
        short co;   //colat in hundredths of degree
        char ao[8]; //azimuth offsets for oblique axes (log form)
        short nm;   //retardation in nanometers
        char rq;    //retardation quality 100 - (blue+red phase error in degree)
        char gq;    //geometric quality 100=perfect 0=not usable
    }   dataStruct;

    dataStruct * mData = new dataStruct[mWidth * mHeight];
    FILE *fp = fopen(filepath_binary.c_str(), "rb");
    if (fp == NULL)
    {
        std::cout<<"Not able to open "<<filepath_binary<<std::endl;
        return 1;
    }
    int count = fread(mData, sizeof(dataStruct), mWidth * mHeight, fp);

    if(count!=mWidth * mHeight)
    {
        std::cout<<"File "<<filepath_binary<<" haven't been read correctly!"<<std::endl;
        return -1;
    }
    else std::cout<<"Reading file "<<filepath_binary<<"."<<std::endl;
    std::cout << "instrument G50: " << instrument << std::endl;
    fclose(fp);

    float * converted_ao = new float[mWidth * mHeight];

    vigra::FImage az_image(mWidth, mHeight);
    vigra::FImage co_image(mWidth, mHeight);
    quality_image.resize(mWidth, mHeight);
    vigra::TinyVector<float, 3> tiny_vector;
    vector_image.resize(mWidth, mHeight);
//    vector_image2.resize(mWidth, mHeight);
    /*

    vigra::FImage nm_image(mWidth, mHeight);
    vigra::FImage rq_image(mWidth, mHeight);
    vigra::FImage gq_image(mWidth, mHeight);
    vigra::FImage quality_image(mWidth, mHeight);
    */
    
    std::cout<<"Start conversion... ";

    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {
            int z=y*mWidth+x;

            //convert pixel data elements
            az_image(x,y)=mData[z].az/90.0;
            co_image(x,y)=mData[z].co/100.0;

            float v;
            float b;
            b=atof(mData[z].ao);
            if (b<=127)
            {
                v = b / 127.0f;
                converted_ao[z] = 10.0f * pow(10,v) - 10.0f;
            }
            else
            {
                v = (255.0f - b) / 127.0f;
                converted_ao[z] = -(10.0f * pow(10,v) - 10.0f);
            }

            //Write quality image
            if(mData[z].gq<gqmin || mData[z].rq<rqmin)
            quality_image(x,y)=0;
                else quality_image(x,y)=255;
        }
/*
    if (vertical_cut) //vertical thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[2]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //z
            tiny_vector[1]=cos(co_image(x,y)*M_PI/180);                    //y
            vector_image2(x, y) = tiny_vector;    
        }
    else  //horizontal thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[1]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //y
            tiny_vector[2]=-cos(co_image(x,y)*M_PI/180);                    //z
            vector_image2(x, y) = tiny_vector;    
        }
*/ 
    //apply median filter
//    median_filter(az_image);
    median_filter(co_image);
    if (vertical_cut) //vertical thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[2]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //z
            tiny_vector[1]=cos(co_image(x,y)*M_PI/180);                    //y
            vector_image(x, y) = tiny_vector;    
        }
    else  //horizontal thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[1]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //y
            tiny_vector[2]=-cos(co_image(x,y)*M_PI/180);                    //z
            vector_image(x, y) = tiny_vector;    
        }
        
    std::cout << "done" << std::endl;
        
    delete converted_ao;
    delete mData;
    
    return 0;
}

int import_data_g60Mainz()
{
    std::string filepath_info = sourcepath+"/info.txt";
    std::string filepath_binary = sourcepath+"/data.cis";
    
    std::string info[15];
  
    std::ifstream infofile (filepath_info.c_str());
    if (infofile.is_open())
    {
        for (int line=0; line<=13; line++)
        {
            getline (infofile, info[line]);
        }
        infofile.close();
    }
    else
    {
        //std::cout << "Unable to open " << filepath_info << std::endl;
        return 1;
    }
    
    instrument.assign(info[1].begin()+13, info[1].end()-1);
    if (instrument != "G6001 Mainz")
	return 1;
    
    date.assign(info[4].begin()+7, info[4].end());
    mineral.assign(info[5].begin()+9, info[5].end());
    std::istringstream(std::string (info[6].begin()+14, info[6].end()-7)) >> mWidth;
    std::istringstream(std::string (info[7].begin()+15, info[7].end()-7)) >> mHeight;
    std::istringstream(std::string (info[8].begin()+12, info[8].end()-9)) >> pixelsize; //micrometer square
    std::istringstream(std::string (info[10].begin()+13, info[10].end())) >> tiles_wide;
    std::istringstream(std::string (info[11].begin()+13, info[11].end())) >> tiles_high;
    std::istringstream(std::string (info[12].begin()+16, info[12].end()-2)) >> scanned_width;
    std::istringstream(std::string (info[13].begin()+17, info[13].end()-2)) >> scanned_height;
    
    tile_size = mWidth/tiles_wide;
    
    
    
    

    //Load and convert binary file
    typedef struct
    {
        short az;   //azimuth in nintieths of degree
        short co;   //colat in hundredths of degree
        char ao[8]; //azimuth offsets for oblique axes (log form)
        short nm;   //retardation in nanometers
        char rq;    //retardation quality 100 - (blue+red phase error in degree)
        char gq;    //geometric quality 100=perfect 0=not usable
    }   dataStruct;

    dataStruct * mData = new dataStruct[mWidth * mHeight];
    FILE *fp = fopen(filepath_binary.c_str(), "rb");
    if (fp == NULL)
    {
        std::cout<<"Not able to open "<<filepath_binary<<std::endl;
        return 1;
    }
    int count = fread(mData, sizeof(dataStruct), mWidth * mHeight, fp);

    if(count!=mWidth * mHeight)
    {
        std::cout<<"File "<<filepath_binary<<" haven't been read correctly!"<<std::endl;
        return -1;
    }
    else std::cout<<"Reading file "<<filepath_binary<<"."<<std::endl;
    std::cout << "instrument G60: " << instrument << std::endl;
    fclose(fp);

    float * converted_ao = new float[mWidth * mHeight];

    vigra::FImage az_image(mWidth, mHeight);
    vigra::FImage co_image(mWidth, mHeight);
    quality_image.resize(mWidth, mHeight);
    vigra::TinyVector<float, 3> tiny_vector;
    vector_image.resize(mWidth, mHeight);
//    vector_image2.resize(mWidth, mHeight);
    /*

    vigra::FImage nm_image(mWidth, mHeight);
    vigra::FImage rq_image(mWidth, mHeight);
    vigra::FImage gq_image(mWidth, mHeight);
    vigra::FImage quality_image(mWidth, mHeight);
    */
    
    std::cout<<"Start conversion... ";

    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {
            int z=y*mWidth+x;

            //convert pixel data elements
            az_image(x,y)=mData[z].az/90.0;
            co_image(x,y)=mData[z].co/100.0;

            float v;
            float b;
            b=atof(mData[z].ao);
            if (b<=127)
            {
                v = b / 127.0f;
                converted_ao[z] = 10.0f * pow(10,v) - 10.0f;
            }
            else
            {
                v = (255.0f - b) / 127.0f;
                converted_ao[z] = -(10.0f * pow(10,v) - 10.0f);
            }

            //Write quality image
            if(mData[z].gq<gqmin || mData[z].rq<rqmin)
            quality_image(x,y)=0;
                else quality_image(x,y)=255;
        }
/*
    if (vertical_cut) //vertical thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[2]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //z
            tiny_vector[1]=cos(co_image(x,y)*M_PI/180);                    //y
            vector_image2(x, y) = tiny_vector;    
        }
    else  //horizontal thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[1]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //y
            tiny_vector[2]=-cos(co_image(x,y)*M_PI/180);                    //z
            vector_image2(x, y) = tiny_vector;    
        }
*/ 
    //apply median filter
//    median_filter(az_image);
    median_filter(co_image);
    if (vertical_cut) //vertical thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[2]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //z
            tiny_vector[1]=cos(co_image(x,y)*M_PI/180);                    //y
            vector_image(x, y) = tiny_vector;    
        }
    else  //horizontal thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[1]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //y
            tiny_vector[2]=-cos(co_image(x,y)*M_PI/180);                    //z
            vector_image(x, y) = tiny_vector;    
        }
        
    std::cout << "done" << std::endl;
        
    delete converted_ao;
    delete mData;
    
    return 0;
}



int import_data_g50LGGE()
{
    std::string filepath_data = sourcepath+"/orientation.dat";
    std::string filepath_image = sourcepath+"/orientation.bmp";
    
    
    std::string info[16];
    std::string dataline;
  
    std::ifstream datafile (filepath_data.c_str());
    if (datafile.is_open())
    {
        std::cout<<"Reading file "<<filepath_data<<"."<<std::endl;
        for (int line=0; line<=15; line++)
        {
            getline (datafile, info[line]);
        }
    }
    else
    {
        //std::cout << "Unable to open " << filepath_data << std::endl;
        return 1;
    }
    
    int pixels;
    
    instrument = "G50 LGGE";
    date.assign(info[3].begin()+5, info[3].end());
    mineral = "unknown";
    mWidth = vigra::ImageImportInfo(filepath_image.c_str()).width();
    mHeight = vigra::ImageImportInfo(filepath_image.c_str()).height();
    std::istringstream(std::string (info[5].begin()+10, info[5].end()-9)) >> pixelsize; //micrometer square
    std::istringstream(std::string (info[13].begin()+6, info[13].end())) >> pixels;
    tile_size = 232;
    tiles_wide = mWidth/tile_size;
    tiles_high = mHeight/tile_size;
    scanned_width = pixelsize*mWidth/1000.f;
    scanned_height = pixelsize*mHeight/1000.f;
    
    if(pixels!=mWidth * mHeight)
    {
        std::cout<<"File "<<filepath_data<<" haven't been read correctly!"<<std::endl;
        return -1;
    }
    
    int * gq = new int[mWidth * mHeight];
    
    vigra::FImage az_image(mWidth, mHeight);
    vigra::FImage co_image(mWidth, mHeight);
    quality_image.resize(mWidth, mHeight);
    vigra::TinyVector<float, 3> tiny_vector;
    vector_image.resize(mWidth, mHeight);
//    vector_image2.resize(mWidth, mHeight);    
    
    std::cout<<"Start conversion... ";

    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {
            int z=y*mWidth+x;
    
            getline (datafile, dataline);
            
            size_t space[3];
            space[1] = dataline.find(" ");
            space[2] = dataline.find(" ", space[1]+1);
                
            std::istringstream(std::string (dataline.begin(), dataline.begin()+space[1])) >> az_image(x,y);            
            std::istringstream(std::string (dataline.begin()+space[1], dataline.begin()+space[2])) >> co_image(x,y);
            std::istringstream(std::string (dataline.begin()+space[2], dataline.end())) >> gq[z];
            
            //Write quality image
            if (gq[z]<gqmin)
                quality_image(x,y)=0;
            else quality_image(x,y)=255;
        }
/*
    if (vertical_cut) //vertical thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[2]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //z
            tiny_vector[1]=cos(co_image(x,y)*M_PI/180);                    //y
            vector_image2(x, y) = tiny_vector;    
        }
    else  //horizontal thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[1]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //y
            tiny_vector[2]=-cos(co_image(x,y)*M_PI/180);                    //z
            vector_image2(x, y) = tiny_vector;    
        }
*/ 
    //apply median filter
//    median_filter(az_image);
    median_filter(co_image);
        
//    vigra::discMedian(srcImageRange(src), destImage(co_image), 3);
    
    if (vertical_cut) //vertical thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[2]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //z
            tiny_vector[1]=cos(co_image(x,y)*M_PI/180);                    //y
            vector_image(x, y) = tiny_vector;    
        }
    else  //horizontal thin section
      for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {    
            //Transform az and co data in carthesian coordinates system:
                //x->east, y->north, z->up
            tiny_vector[0]=sin(co_image(x,y)*M_PI/180)*cos(az_image(x,y)*M_PI/180);    //x
            tiny_vector[1]=sin(co_image(x,y)*M_PI/180)*sin(az_image(x,y)*M_PI/180);    //y
            tiny_vector[2]=-cos(co_image(x,y)*M_PI/180);                    //z
            vector_image(x, y) = tiny_vector;    
        }
    
    datafile.close();
    std::cout << "done" << std::endl;
    
    delete gq;
    
    return 0;
}

int delete_tile_borders(vigra::UInt32Image &label_image, int tile_borders[][2])
{
    std::cout << "Eliminate tile borders... ";

    int good_value, bad_value, xx, yy, count;
    vigra::UInt32Image image(mWidth, mHeight);
    image = label_image;

    for (int pix=0; pix<borderpixel; pix++)
    {
        xx = tile_borders[pix][0];
        yy = tile_borders[pix][1];
        if (image(xx,yy)!=0 && image(xx-1,yy)!=0 && image(xx,yy)!=image(xx-1,yy))
        { 
            good_value = image(xx,yy);
            bad_value = image(xx-1,yy);
            for (int x=0; x<mWidth; x++) 
            for (int y=0; y<mHeight; y++)
                if (image(x,y)==bad_value)
                    image(x,y)=good_value;
        }
        if (image(xx,yy)!=0 && image(xx,yy-1)!=0 && image(xx,yy)!=image(xx,yy-1))
        { 
            good_value = image(xx,yy);
            bad_value = image(xx,yy-1);
            for (int x=0; x<mWidth; x++) 
            for (int y=0; y<mHeight; y++)
                if (image(x,y)==bad_value)
                    image(x,y)=good_value;
        }
    }
    label_image = image;
    
    std::cout << "done" << std::endl;  
    
    return 0;
}

int offset_vector_image(vigra::FVector3Image &image, int tile_borders[][2])
{
  std::cout << "offset tiles in vector_image...";
  
  int offsetx = 14;
  int new_x, new_y;
  borderpixel = 0;
  vigra::FVector3Image new_image(mWidth, mHeight);
  new_image = image;
  
  for (int ytile = 0; ytile < tiles_high; ytile+=2)
    for (int xtile = 1; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
	  new_x = x + tile_size*xtile - offsetx;
	  new_y = y + tile_size*ytile;
	  if (new_x >= 0 && new_x < mWidth)
	  {
                new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
                if ((x==0 || y==0) && new_x!=0 && new_y!=0)
                {
                    tile_borders[borderpixel][0]=new_x;
                    tile_borders[borderpixel][1]=new_y;
                    borderpixel++;
                }
          }
	}
  for (int ytile = 1; ytile < tiles_high; ytile+=2)
    for (int xtile = 0; xtile < tiles_wide-1; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
	  new_x = x + tile_size*xtile;
	  new_y = y + tile_size*ytile;
	  if (new_x >= 0 && new_x < mWidth)
	  {
                new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
                if ((x==0 || y==0) && new_x!=0 && new_y!=0)
                {
                    tile_borders[borderpixel][0]=new_x;
                    tile_borders[borderpixel][1]=new_y;
                    borderpixel++;
                }
          }
        }
  for (int ytile = 1; ytile < tiles_high; ytile+=2)
    for (int xtile = tiles_wide-1; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
	  new_x = x + tile_size*xtile - offsetx;
	  new_y = y + tile_size*ytile;
	  if (new_x >= 0 && new_x < mWidth)
	  {
                new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
                if ((x==0 || y==0) && new_x!=0 && new_y!=0)
                {
                    tile_borders[borderpixel][0]=new_x;
                    tile_borders[borderpixel][1]=new_y;
                    borderpixel++;
                }
          }
	}
  
    image = new_image;
    std::cout << " done" << std::endl;
    return 0;
}

int offset_quality_image(vigra::BImage &image)
{
    std::cout << "offset tiles in quality_image...";
  
    int offsetx = 14;
    int new_x, new_y;
    vigra::BImage new_image(mWidth, mHeight);
    new_image = image;
    
    
    for (int ytile = 0; ytile < tiles_high; ytile+=2)
    for (int xtile = 1; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
	  new_x = x + tile_size*xtile - offsetx;
	  new_y = y + tile_size*ytile;
	  if (new_x >= 0 && new_x < mWidth)
	    new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
        }
    for (int ytile = 1; ytile < tiles_high; ytile+=2)
    for (int xtile = 0; xtile < tiles_wide-1; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
	  new_x = x + tile_size*xtile;
	  new_y = y + tile_size*ytile;
	  if (new_x >= 0 && new_x < mWidth)
	    new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
        }
    for (int ytile = 1; ytile < tiles_high; ytile+=2)
    for (int xtile = tiles_wide-1; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
	  new_x = x + tile_size*xtile - offsetx;
	  new_y = y + tile_size*ytile;
	  if (new_x >= 0 && new_x < mWidth)
	    new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
        }
	    
        
    image = new_image;
    std::cout << " done" << std::endl;
    return 0;
}

int offset_topo_image(vigra::FImage &image)
{
    std::cout << "offset tiles in topo_image...";
  
    int offsetx = 14;
    int new_x, new_y;
    vigra::FImage new_image(mWidth, mHeight);
    new_image = image;
    
    for (int ytile = 0; ytile < tiles_high; ytile+=2)
    for (int xtile = 1; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
	  new_x = x + tile_size*xtile - offsetx;
	  new_y = y + tile_size*ytile;
	  if (new_x >= 0 && new_x < mWidth)
	    new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
        }
    for (int ytile = 1; ytile < tiles_high; ytile+=2)
    for (int xtile = 0; xtile < tiles_wide-1; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
	  new_x = x + tile_size*xtile;
	  new_y = y + tile_size*ytile;
	  if (new_x >= 0 && new_x < mWidth)
	    new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
        }
    for (int ytile = 1; ytile < tiles_high; ytile+=2)
    for (int xtile = tiles_wide-1; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
	  new_x = x + tile_size*xtile - offsetx;
	  new_y = y + tile_size*ytile;
	  if (new_x >= 0 && new_x < mWidth)
	    new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
        }
        
    image = new_image;
    std::cout << " done" << std::endl;
    return 0;
}

int rotate_vector_image(vigra::FVector3Image &image, int tile_borders[][2])
{
    std::cout << "rotate tiles in vector_image...";
  
    int translat1_x, translat1_y, translat2_x, translat2_y, new_x, new_y;
    vigra::FVector3Image new_image(mWidth, mHeight);
    
    translat1_y=round(tile_size*sin(rot_angle*M_PI/180));
    translat1_x=-round(tile_size*(1-cos(rot_angle*M_PI/180)));
    translat2_x = -translat1_y;
    translat2_y = translat1_x;

    borderpixel = 0;
    
    for (int ytile = 0; ytile < tiles_high; ytile++)
    for (int xtile = 0; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
            new_x=x + tile_size*xtile + xtile*translat1_x + ytile*translat2_x;
            new_y=y + tile_size*ytile + xtile*translat1_y + ytile*translat2_y;
            if (new_x >= 0 && new_y >= 0 && new_x < mWidth && new_y < mHeight)
            {
                new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
                if ((x==0 || y==0) && new_x!=0 && new_y!=0)
                {
                    tile_borders[borderpixel][0]=new_x;
                    tile_borders[borderpixel][1]=new_y;
                    borderpixel++;
                }
            }
        }
    image = new_image;
    std::cout << " done" << std::endl;
    return 0;
}

int rotate_boolean_image(vigra::BImage &image)
{
    std::cout << "rotate tiles in quality_image...";
  
    int translat1_x, translat1_y, translat2_x, translat2_y, new_x, new_y;
    vigra::BImage new_image(mWidth, mHeight);
    
    //set the interior of new_image true in order to avoid rectangular gaps between tiles
    int margin_band = int(tile_size/2);
    for (int y = margin_band; y < (mHeight-margin_band); y++)
        for (int x = margin_band; x < (mWidth-margin_band); x++)
            new_image(x,y) = 1;
    
        
    translat1_y=round(tile_size*sin(rot_angle*M_PI/180));
    translat1_x=-round(tile_size*(1-cos(rot_angle*M_PI/180)));
    translat2_x = -translat1_y;
    translat2_y = translat1_x;

    
    for (int ytile = 0; ytile < tiles_high; ytile++)
    for (int xtile = 0; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
            new_x=x + tile_size*xtile + xtile*translat1_x + ytile*translat2_x;
            new_y=y + tile_size*ytile + xtile*translat1_y + ytile*translat2_y;
            if (new_x >= 0 && new_y >= 0 && new_x < mWidth && new_y < mHeight)
            new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
        }
    image = new_image;
    std::cout << " done" << std::endl;
    return 0;
}

int rotate_topo_image(vigra::FImage &image)
{
    std::cout << "rotate tiles in topo_image...";
  
    int translat1_x, translat1_y, translat2_x, translat2_y, new_x, new_y;
    vigra::FImage new_image(mWidth, mHeight);
    
    translat1_y=round(tile_size*sin(rot_angle*M_PI/180));
    translat1_x=-round(tile_size*(1-cos(rot_angle*M_PI/180)));
    translat2_x = -translat1_y;
    translat2_y = translat1_x;
    
    for (int ytile = 0; ytile < tiles_high; ytile++)
    for (int xtile = 0; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {
            new_x=x + tile_size*xtile + xtile*translat1_x + ytile*translat2_x;
            new_y=y + tile_size*ytile + xtile*translat1_y + ytile*translat2_y;
            if (new_x >= 0 && new_y >= 0 && new_x < mWidth && new_y < mHeight)
            new_image(new_x, new_y) = image(x+tile_size*xtile,y+tile_size*ytile);
        }
    image = new_image;
    std::cout << " done" << std::endl;
    return 0;
}

int export_topo_image(vigra::FImage topo_image, std::string path)
{
    float max_value;
    max_value = maximal_misorientation;
    vigra::FImage new_image(mWidth, mHeight);
    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {
            if(topo_image(x,y)>maximal_misorientation) 
                new_image(x,y) = maximal_misorientation;
            
            else new_image(x,y) = topo_image(x,y);
        }
        
    exportImage(srcImageRange(new_image), vigra::ImageExportInfo(path.c_str()));
    return 0;    
}

int export_grains_bw(vigra::IImage grain_label_image, std::string path)
{
    vigra::IImage grain_image(mWidth,mHeight);
  
    // create grain_image
    for (int y = 0; y < mHeight; y++)
      for (int x = 0; x < mWidth; x++)
    if (grain_label_image(x,y)==0)
      grain_image(x,y) = 0;
    else
      grain_image(x,y) = 255;
  
    exportImage(srcImageRange(grain_image), vigra::ImageExportInfo(path.c_str()));
    return 0;
}

int export_grains_rgb(vigra::IImage grain_label_image, std::vector<Grains> &Grain, std::string path)
{
    vigra::FRGBImage color_image(mWidth, mHeight, black);
    float azim,colat;
    int x,y;

    for (int i=1; i<=nr_grains; i++)
        for (int pixel_iterator=0; pixel_iterator<Grain[i].pointer.size(); pixel_iterator++)
        {
            azim=Grain[i].az;
            colat=Grain[i].co;
            x = Grain[i].pointer[pixel_iterator].x;
            y = Grain[i].pointer[pixel_iterator].y;
            
            if (azim>=0.0 && azim<120.0)
            {
                color_image(x,y)[0]=azim/120;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=(120.0-azim)/120;
            }
            if (azim>=120.0 && azim<240.0)
            {
                color_image(x,y)[0]=(240.0-azim)/120;
                color_image(x,y)[1]=(azim-120.0)/120;
                color_image(x,y)[2]=0.0;
            }
            if (azim>=240.0 && azim<360.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=(360.0-azim)/120;
                color_image(x,y)[2]=(azim-240.0)/120;
            }
            color_image(x,y)+=pow(cos(colat*M_PI/180),5)*(white-color_image(x,y));
        }
    
    exportImage(srcImageRange(color_image), vigra::ImageExportInfo(path.c_str()));    
    return 0;
}

int export_grains_trendw(vigra::IImage grain_label_image, std::vector<Grains> &Grain, std::string path)
{
    vigra::FRGBImage color_image(mWidth, mHeight);
    float azim,colat;
    int x,y;

    for (int i=1; i<=nr_grains; i++)
        for (int pixel_iterator=0; pixel_iterator<Grain[i].pointer.size(); pixel_iterator++)
        {
            azim=Grain[i].az;
            colat=Grain[i].co;
            x = Grain[i].pointer[pixel_iterator].x;
            y = Grain[i].pointer[pixel_iterator].y;
            
            if (azim>=0.0 && azim<30.0)
            {
                color_image(x,y)[0]=230.0/255;
                color_image(x,y)[1]=azim * 23/3/255;
                color_image(x,y)[2]=0.0;
            }
            else if (azim>=30.0 && azim<60.0)
            {
                color_image(x,y)[0]=(60.0-azim) * 23/3/255;
                color_image(x,y)[1]=230.0/255;
                color_image(x,y)[2]=0.0;
            }
            else if (azim>=60.0 && azim<90.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=230.0/255;
                color_image(x,y)[2]=(azim-60.0) * 23/3/255;
            }
            else if (azim>=90.0 && azim<120.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=(120.0-azim) * 23/3/255;
                color_image(x,y)[2]=230.0/255;
            }
            else if (azim>=120.0 && azim<150.0)
            {
                color_image(x,y)[0]=(azim-120.0) * 23/3/255;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=230.0/255;
            }
            else if (azim>=150.0 && azim<180.0)
            {
                color_image(x,y)[0]=230.0/255;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=(180.0-azim) * 23/3/255;
            }
            else if (azim>=180.0 && azim<210.0)
            {
                color_image(x,y)[0]=230.0/255;
                color_image(x,y)[1]=(azim-180.0) * 23/3/255;
                color_image(x,y)[2]=0.0;
            }
            else if (azim>=210.0 && azim<240.0)
            {
                color_image(x,y)[0]=(240.0-azim) * 23/3/255;
                color_image(x,y)[1]=230.0/255;
                color_image(x,y)[2]=0.0;
            }
            else if (azim>=240.0 && azim<270.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=230.0/255;
                color_image(x,y)[2]=(azim-240.0) * 23/3/255;
            }
            else if (azim>=270.0 && azim<300.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=(300.0-azim) * 23/3/255;
                color_image(x,y)[2]=230.0/255;
            }
            else if (azim>=300.0 && azim<330.0)
            {
                color_image(x,y)[0]=(azim-300.0) * 23/3/255;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=230.0/255;
            }
            else if (azim>=330.0 && azim<360.0)
            {
                color_image(x,y)[0]=230.0/255;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=(360.0-azim) * 23/3/255;
            }
            color_image(x,y)+=pow(cos(colat*M_PI/180),5)*(white-color_image(x,y));
        }

    exportImage(srcImageRange(color_image), vigra::ImageExportInfo(path.c_str()));    
    return 0;
}

int export_boundary_rgb(vigra::IImage boundary_label_image, vigra::IImage regions, vigra::BImage bground_image, std::vector<Segment> &Segments, std::vector<Grains> &Grain, std::vector<Boundaries> &Boundary, std::string path)
{
    vigra::FRGBImage color_image(mWidth, mHeight, black);
    float azim,colat;
    int grain_index, segment_index;

    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
	{
        segment_index = boundary_label_image(x,y);

        if (Segments[segment_index].phase == 1)
        {
            
            grain_index = Segments[segment_index].child_index;
            azim=Grain[grain_index].az;
            colat=Grain[grain_index].co;
        
            if (azim>=0.0 && azim<120.0)
            {
              color_image(x,y)[0]=azim/120;
              color_image(x,y)[1]=0.0;
              color_image(x,y)[2]=(120.0-azim)/120;
            }
            if (azim>=120.0 && azim<240.0)
            {
              color_image(x,y)[0]=(240.0-azim)/120;
              color_image(x,y)[1]=(azim-120.0)/120;
              color_image(x,y)[2]=0.0;
            }
            if (azim>=240.0 && azim<360.0)
            {
              color_image(x,y)[0]=0.0;
              color_image(x,y)[1]=(360.0-azim)/120;
              color_image(x,y)[2]=(azim-240.0)/120;
            }
            color_image(x,y)+=pow(cos(colat*M_PI/180),5)*(white-color_image(x,y));
        }
	}

    int dd = 1; //grain boundary thickness in the image
        
    for (int y=dd; y<mHeight-dd; y++)
        for (int x=dd; x<mWidth-dd; x++)
	{
        if (regions(x,y)==0)
        {
	    for (int dy=-dd; dy<=dd; dy++)
            for (int dx=-dd; dx<=dd; dx++)
            color_image(x+dx,y+dy)=black;
        }
	}

    
    exportImage(srcImageRange(color_image), vigra::ImageExportInfo(path.c_str()));    
    return 0;
}

int export_boundary_trendw(vigra::IImage boundary_label_image, vigra::IImage regions, vigra::BImage bground_image, std::vector<Segment> &Segments, std::vector<Grains> &Grain, std::vector<Boundaries> &Boundary, std::string path)
{
    vigra::FRGBImage color_image(mWidth, mHeight);
    float azim,colat;
    int grain_index, segment_index;

    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
		{
        segment_index = boundary_label_image(x,y);

        if (Segments[segment_index].phase == 1)
        {
            grain_index = Segments[segment_index].child_index;
            azim=Grain[grain_index].az;
            colat=Grain[grain_index].co;
        
            if (azim>=0.0 && azim<30.0)
            {
                color_image(x,y)[0]=230.0/255;
                color_image(x,y)[1]=azim * 23/3/255;
                color_image(x,y)[2]=0.0;
            }
            else if (azim>=30.0 && azim<60.0)
            {
                color_image(x,y)[0]=(60.0-azim) * 23/3/255;
                color_image(x,y)[1]=230.0/255;
                color_image(x,y)[2]=0.0;
            }
            else if (azim>=60.0 && azim<90.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=230.0/255;
                color_image(x,y)[2]=(azim-60.0) * 23/3/255;
            }
            else if (azim>=90.0 && azim<120.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=(120.0-azim) * 23/3/255;
                color_image(x,y)[2]=230.0/255;
            }
            else if (azim>=120.0 && azim<150.0)
            {
                color_image(x,y)[0]=(azim-120.0) * 23/3/255;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=230.0/255;
            }
            else if (azim>=150.0 && azim<180.0)
            {
                color_image(x,y)[0]=230.0/255;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=(180.0-azim) * 23/3/255;
            }
            else if (azim>=180.0 && azim<210.0)
            {
                color_image(x,y)[0]=230.0/255;
                color_image(x,y)[1]=(azim-180.0) * 23/3/255;
                color_image(x,y)[2]=0.0;
            }
            else if (azim>=210.0 && azim<240.0)
            {
                color_image(x,y)[0]=(240.0-azim) * 23/3/255;
                color_image(x,y)[1]=230.0/255;
                color_image(x,y)[2]=0.0;
            }
            else if (azim>=240.0 && azim<270.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=230.0/255;
                color_image(x,y)[2]=(azim-240.0) * 23/3/255;
            }
            else if (azim>=270.0 && azim<300.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=(300.0-azim) * 23/3/255;
                color_image(x,y)[2]=230.0/255;
            }
            else if (azim>=300.0 && azim<330.0)
            {
                color_image(x,y)[0]=(azim-300.0) * 23/3/255;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=230.0/255;
            }
            else if (azim>=330.0 && azim<360.0)
            {
                color_image(x,y)[0]=230.0/255;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=(360.0-azim) * 23/3/255;
            }
            color_image(x,y)+=pow(cos(colat*M_PI/180),5)*(white-color_image(x,y));
        }
        if (bground_image(x,y)==0) color_image(x,y)=black;  //border to background
	}

    int dd = 1; //grain boundary thickness in the image
        
    for (int y=dd; y<mHeight-dd; y++)
        for (int x=dd; x<mWidth-dd; x++)
	{
        if (regions(x,y)==0)
        {
	    for (int dy=-dd; dy<=dd; dy++)
            for (int dx=-dd; dx<=dd; dx++)
            color_image(x+dx,y+dy)=black;
        }
    }
    
    exportImage(srcImageRange(color_image), vigra::ImageExportInfo(path.c_str()));    
    return 0;
}


int export_regions(vigra::IImage regions, std::vector<Boundaries> &Boundary, std::string path) //experiment
{
    vigra::FRGBImage image(mWidth, mHeight);
    int xx, yy;    
    image = blue;
  
    for (int i=1; i<=nr_boundaries; i++)
//      if (Boundary[i].cross_az > 350 || Boundary[i].cross_az < 10 || fabs(180.f-Boundary[i].cross_az)<10.f)
        if (Boundary[i].tilt_to_boundary < 10.f)
            for (int j=0; j<Boundary[i].pointer.size(); j++)
            {
                xx = abs(Boundary[i].pointer[j].x);
                yy = abs(Boundary[i].pointer[j].y);
                image(xx,yy)=green;
            }
//      else if (fabs(90.f-Boundary[i].cross_az)<10.f || fabs(270.f-Boundary[i].cross_az)<10.f)
        else if (Boundary[i].tilt_to_boundary > 80.f)
            for (int j=0; j<Boundary[i].pointer.size(); j++)
            {
                xx = abs(Boundary[i].pointer[j].x);
                yy = abs(Boundary[i].pointer[j].y);
                image(xx,yy)=white;
            }
        else
        for (int j=0; j<Boundary[i].pointer.size(); j++)
        {
            xx = abs(Boundary[i].pointer[j].x);
            yy = abs(Boundary[i].pointer[j].y);
            image(xx,yy)=black;
        }
      
    exportImage(srcImageRange(regions), vigra::ImageExportInfo(path.c_str()));
    return 0;
}

int export_vectorimage(vigra::FVector3Image vector_image, std::string path)
{
    vigra::FRGBImage color_image(mWidth, mHeight);
    float azim,colat;
  
    // Transform to geo coordinates in horizontal projection
    for (int y=0; y<mHeight; y++)
    {
        for (int x=0; x<mWidth; x++)
        {
            if (vector_image(x,y)[2]>0.0)
                vector_image(x,y)=-vector_image(x,y);    //rotate all grain axis vectors in negative z-direction ("down") - projection onto the lower hemisphere
            if (vector_image(x,y)[1]>0.0 && vector_image(x,y)[0]>=0.0)
                azim=atan(vector_image(x,y)[0]/vector_image(x,y)[1])*180/M_PI;
            else if (vector_image(x,y)[1]<0.0)
                azim=(atan(vector_image(x,y)[0]/vector_image(x,y)[1])+M_PI)*180/M_PI;
            else if (vector_image(x,y)[1]>0.0 && vector_image(x,y)[0]<0.0)
                azim=(atan(vector_image(x,y)[0]/vector_image(x,y)[1])+2*M_PI)*180/M_PI;
            else if (vector_image(x,y)[0]>0.0)   azim=90;
            else if (vector_image(x,y)[0]<0.0)   azim=270;
            else    azim=0;
            colat=acos(-vector_image(x,y)[2]/sqrt(vigra::squaredNorm(vector_image(x,y))))*180/M_PI; //acos(-z) because lower hemisphere projection



            if (azim>=0.0 && azim<120.0)
            {
                color_image(x,y)[0]=azim/120;
                color_image(x,y)[1]=0.0;
                color_image(x,y)[2]=(120.0-azim)/120;
            }
            if (azim>=120.0 && azim<240.0)
            {
                color_image(x,y)[0]=(240.0-azim)/120;
                color_image(x,y)[1]=(azim-120.0)/120;
                color_image(x,y)[2]=0.0;
            }
            if (azim>=240.0 && azim<360.0)
            {
                color_image(x,y)[0]=0.0;
                color_image(x,y)[1]=(360.0-azim)/120;
                color_image(x,y)[2]=(azim-240.0)/120;
            }
            color_image(x,y)+=pow(cos(colat*M_PI/180),5)*(white-color_image(x,y));
        }
    }

    exportImage(srcImageRange(color_image), vigra::ImageExportInfo(path.c_str()));
    
    return 0;
}

int export_vectorimage_bin(std::string path) //obsolete
{
    FILE *fp;
    std::string bin_path=path;
    bin_path.resize(bin_path.size()-4);
    bin_path.append(".bin");
    fp =fopen(bin_path.c_str(),"wb");

    FILE *fp2;
    bin_path=path;
    bin_path.resize(bin_path.size()-4);
    bin_path.append("_no_filter.bin");
    fp2 =fopen(bin_path.c_str(),"wb");
    
       for (int y=0; y<mHeight; y++)
       {
        for (int x=0; x<mWidth; x++)
        {
            if (vector_image(x,y)[2]>0.0)
                vector_image(x,y)=-vector_image(x,y);    //rotate all grain axis vectors in negative z-direction ("down")
            
                
            fwrite(&vector_image(x,y)[0],sizeof(float),1,fp);
            fwrite(&vector_image(x,y)[1],sizeof(float),1,fp);
            fwrite(&vector_image(x,y)[2],sizeof(float),1,fp);
/*	    
	    if (vector_image2(x,y)[2]>0.0)
                vector_image2(x,y)=-vector_image2(x,y);    //rotate all grain axis vectors in negative z-direction ("down")

            fwrite(&vector_image2(x,y)[0],sizeof(float),1,fp2);
            fwrite(&vector_image2(x,y)[1],sizeof(float),1,fp2);
            fwrite(&vector_image2(x,y)[2],sizeof(float),1,fp2);
*/
	}
       }
	   
    fclose(fp);
    fclose(fp2);
    
    return 0;
}

int filter_low_quality(vigra::UInt32Image &label_image, vigra::BImage quality_image)
{
    std::cout << "filter low quality pixels.";
    vigra::UInt32Image old_image(mWidth, mHeight);
    vigra::UInt32Image new_image(mWidth, mHeight);
    old_image = label_image;

    // erase pixels with low rq and gq
    for (int x=0; x<mWidth; x++)
        for (int y=0; y<mHeight; y++)
            if (quality_image(x,y)==0)
            old_image(x,y) = 0;

    std::cout << ".";
    // find segments in label_image
    nr_regions = vigra::labelImageWithBackground(vigra::srcImageRange(old_image), vigra::destImage(new_image), false, 0);

    label_image = new_image;
    std::cout << " done" << std::endl;  
    return 0;
}

int filter_small_grains(vigra::UInt32Image &label_image)
{
    std::cout << "filter small segments.";
    
    vigra::UInt32Image image(mWidth, mHeight);
    vigra::UInt32Image new_image(mWidth, mHeight);
    image = label_image;

  
    // calculate size of segments
    std::vector<int> segment_size(nr_regions+1,0);

    for (int x=0; x<mWidth; x++)
        for (int y=0; y<mHeight; y++)
            segment_size[image(x,y)]++;
    std::cout << ".";

    // erase segments with small segment size
    for (int x=0; x<mWidth; x++)
        for (int y=0; y<mHeight; y++)
            if (segment_size[label_image(x,y)]<minimal_grain_size)
		image(x,y) = 0;


    nr_regions = vigra::labelImageWithBackground(vigra::srcImageRange(image), vigra::destImage(new_image), false, 0);
    label_image=new_image;
        
    std::cout << " done" << std::endl;   
    return 0;
  
}

int cut_margin(vigra::UInt32Image &label_image, vigra::BImage &bground_image)
{
    std::cout << "cut margin.";
  
    int background = 0;
    float density_limit = 0.5;
    int margin_up, margin_down, margin_left, margin_right, pixel_count;
    bool margin_found;
    float density;
    
    vigra::UInt32Image image(mWidth, mHeight);
    vigra::UInt32Image new_image(mWidth, mHeight);
    vigra::BImage bground(mWidth, mHeight);
    image = label_image;
    bground = 1;
    
    margin_found = false;
    for (int y = 0; y < tile_size && margin_found==false; y++)
    {
        pixel_count = 0;
        for (int x = 0; x < mWidth; x++)
        {
            if (label_image(x,y)>0)
            pixel_count++;
        }
        density = (float)pixel_count/(float)mWidth;
        if (density > density_limit)
        {
            margin_found = true;
            margin_up = y;
        }
    }
    if (margin_found == false)
    margin_up = tile_size;
    std::cout << ".";
    
    margin_found = false;
    for (int y = mHeight-1; y >= mHeight-tile_size && margin_found==false; y--)
    {
        pixel_count = 0;
        for (int x = 0; x < mWidth; x++)
        {
            if (label_image(x,y)>0)
            pixel_count++;
        }
        density = (float)pixel_count/(float)mWidth;
        if (density > density_limit)
        {
            margin_found = true;
            margin_down = y;
        }
    }
    if (margin_found == false)
    margin_down = mHeight-tile_size;
    std::cout << ".";    
    
    margin_found = false;
    for (int x = 0; x < tile_size && margin_found==false; x++)
    {
        pixel_count = 0;
        for (int y = 0; y < mHeight; y++)
        {
            if (label_image(x,y)>0)
            pixel_count++;
        }
        density = (float)pixel_count/(float)mHeight;
        if (density > density_limit)
        {
            margin_found = true;
            margin_left = x;
        }
    }
    if (margin_found == false)
    margin_left = tile_size;
    std::cout << ".";
    
    margin_found = false;
    for (int x = mWidth-1; x >= mWidth-tile_size && margin_found==false; x--)
    {
        pixel_count = 0;
        for (int y = 0; y < mHeight; y++)
        {
            if (label_image(x,y)>0)
            pixel_count++;
        }
        density = (float)pixel_count/(float)mHeight;
        if (density > density_limit)
        {
            margin_found = true;
            margin_right = x;
        }
    }
    if (margin_found == false)
    margin_right = mWidth-tile_size;    
    std::cout << ".";    

//    margin_left = tile_size;
    
    for (int y = 0; y < margin_up; y++)
        for (int x = 0; x < mWidth; x++)
        {
            bground(x,y) = background;
        }
      
    for (int y = margin_down+1; y < mHeight; y++)
        for (int x = 0; x < mWidth; x++)
        {
            bground(x,y) = background;
        }
      
    for (int x = 0; x < margin_left; x++)
        for (int y = 0; y < mHeight; y++)
        {
            bground(x,y) = background;
        }
      
    for (int x = margin_right+1; x < mWidth; x++)
        for (int y = 0; y < mHeight; y++)
        {
            bground(x,y) = background;
        }
 
//    label_image=image;
    bground_image = bground;

      
    std::cout << " done" << std::endl;
    return 0;
    
}

int assign_background(vigra::UInt32Image &label_image, vigra::BImage &bground_image)
{
    int background = nr_regions + 1;
    vigra::UInt32Image image(mWidth, mHeight);
    vigra::UInt32Image new_image(mWidth, mHeight);
    image = label_image;
    for (int x=0; x<mWidth; x++)
        for (int y=0; y<mHeight; y++)
            if (bground_image(x,y) == 0)
                image(x,y) = background;
            
    nr_regions = vigra::labelImageWithBackground(vigra::srcImageRange(image), vigra::destImage(new_image), false, 0);
    label_image=new_image;
    
    return 0;
    
}

/*
int smooth(vigra::BImage &quality_image)
{
    vigra::BasicImage< vigra::TinyVector <float, 3> > image(mWidth,mHeight);
    image = vector_image;
    vigra::TinyVector <float, 3> pixel;
    int xx, yy;
    
    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
    {
        pixel = image(x,y);
        if (pixel[2]<0.0)
          image(x,y) = -pixel;
    }
       
    for (int ytile = 0; ytile < tiles_high; ytile++)
    for (int xtile = 0; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {

            xx=x + tile_size*xtile;
            yy=y + tile_size*ytile;

            pixel = image(xx,yy);
            if (x-1 >= 0)
            if (quality_image(xx-1,yy) == 255)
              pixel += image(xx-1,yy);
            if (x+1 < tile_size)
            if (quality_image(xx+1,yy) == 255)
              pixel += image(xx+1,yy);
            if (y-1 >= 0)
            if (quality_image(xx,yy-1) == 255)
              pixel += image(xx,yy-1);
            if (y+1 < tile_size)
            if (quality_image(xx,yy+1) == 255)
              pixel += image(xx,yy+1);
            
            if (x-1 >= 0 && y-1 >= 0)
            if (quality_image(xx-1,yy-1) == 255)
              pixel += image(xx-1,yy-1)/2;
            if (x+1 < tile_size && y-1 >= 0)
            if (quality_image(xx+1,yy-1) == 255)
              pixel += image(xx+1,yy-1)/2;
            if (x-1 >= 0 && y+1 < tile_size)
            if (quality_image(xx-1,yy+1) == 255)
              pixel += image(xx-1,yy+1)/2;
            if (x+1 < tile_size && y+1 < tile_size)
            if (quality_image(xx+1,yy+1) == 255)
              pixel += image(xx+1,yy+1)/2;



        vector_image(xx,yy) = pixel/sqrt(vigra::squaredNorm(pixel));
      

    }
    return 0;
}
*/

int create_bground(vigra::BImage &bground_image, std::string path)
{
    vigra::RGBValue<vigra::UInt8, 0,1,2> bwhite (255,255,255);
    vigra::BRGBImage bground_mask(mWidth,mHeight);
    vigra::ImageImportInfo imageInfo(path.c_str());
//    vigra::MultiArray<2, unsigned int> imageArray(imageInfo.width(), imageInfo.height());
    vigra::importImage(imageInfo, destImage(bground_mask));
    
    for (int y = 0; y < mHeight; y++)
        for (int x = 0; x < mWidth; x++)
        {
            if (bground_mask(x,y) == bwhite)
                bground_image(x,y) = 0; //background
            else
             bground_image(x,y) = 1; //sample
        }
  
    return 0;
}

int create_topo(vigra::FVector3Image &vector_image, vigra::FImage &topo_image)
{
    float dx1, dx2, dy1, dy2;
/*  
        for (int y=1; y<mHeight-1; y++)
        for (int x=1; x<mWidth-1; x++)
        {
        dx1 = fabs(vigra::dot(vector_image(x,y),vector_image(x-1,y)));
        dx2 = fabs(vigra::dot(vector_image(x,y),vector_image(x+1,y)));
        dy1 = fabs(vigra::dot(vector_image(x,y),vector_image(x,y-1)));
        dy2 = fabs(vigra::dot(vector_image(x,y),vector_image(x,y+1)));
        topo_image(x,y) = acos(sqrt((pow((dx1+dx2)/2,2)+pow((dy1+dy2)/2,2))/2))*180/M_PI;
    }
*/      
/*    sqrt(pow(fabs(vigra::dot(vector_image(x,y),vector_image(x-1,y)))
                    +fabs(vigra::dot(vector_image(x,y),vector_image(x+1,y))/2),2)
                   +pow(fabs(vigra::dot(vector_image(x,y),vector_image(x,y-1)))
                    +fabs(vigra::dot(vector_image(x,y),vector_image(x,y+1))/2),2)
                    /2);  */
    
    int xx,yy;
    for (int ytile = 0; ytile < tiles_high; ytile++)
    for (int xtile = 0; xtile < tiles_wide; xtile++)
        for (int y = 0; y < tile_size; y++)
        for (int x = 0; x < tile_size; x++)
        {

            xx=x + tile_size*xtile;
            yy=y + tile_size*ytile;
   
            if (x==0)        dx1 = fabs(vigra::dot(vector_image(xx,yy),vector_image(xx+1,yy)));
            else        dx1 = fabs(vigra::dot(vector_image(xx,yy),vector_image(xx-1,yy)));
            if (x==tile_size-1)    dx2 = fabs(vigra::dot(vector_image(xx,yy),vector_image(xx-1,yy)));
            else        dx2 = fabs(vigra::dot(vector_image(xx,yy),vector_image(xx+1,yy)));
            if (y==0)        dy1 = fabs(vigra::dot(vector_image(xx,yy),vector_image(xx,yy+1)));
            else        dy1 = fabs(vigra::dot(vector_image(xx,yy),vector_image(xx,yy-1)));
            if (y==tile_size-1)    dy2 = fabs(vigra::dot(vector_image(xx,yy),vector_image(xx,yy-1)));
            else        dy2 = fabs(vigra::dot(vector_image(xx,yy),vector_image(xx,yy+1)));
            topo_image(xx,yy) = acos(sqrt((pow((dx1+dx2)/2,2)+pow((dy1+dy2)/2,2))/2))*180/M_PI;
            
        }
        

    return 0;
}

int classify_segments(std::vector<Segment> &Segments, vigra::IImage &grain_label_image, vigra::BImage bground_image)
{
    std::cout << "classify segments" << std::endl;
    for(int i=1; i<=nr_regions; i++)
    {
        Segments[i].phase=0;
        Segments[i].size=0;
        Segments[i].xcenter=0;
        Segments[i].ycenter=0;
        Segments[i].pointer.resize(1);
        Segments[i].pointer[0].x = Segments[i].pointer[0].y = 0;
    }


    //calculate center of mass positions, mean segment size
    point point_xy;

    for (int x=0; x<mWidth; x++)
        for (int y=0; y<mHeight; y++)
        {
            //segment labeling starts with 1
            if (grain_label_image(x,y)>0)
            {
                int i = grain_label_image(x,y);
                if (bground_image(x,y)==1)
                    Segments[i].phase=1;
                
                Segments[i].size++;
                Segments[i].xcenter+=x/float(mWidth);
                Segments[i].ycenter+=y/float(mHeight);
                
                point_xy.x = x;
                point_xy.y = y;
                Segments[i].pointer.push_back(point_xy);
            }
        }

    
    for(int i=1; i<=nr_regions; i++)
    {
        Segments[i].xcenter=Segments[i].xcenter/Segments[i].size*float(mWidth);
        Segments[i].ycenter=Segments[i].ycenter/Segments[i].size*float(mHeight);
        if (Segments[i].phase == 1)
            nr_grains++;
    }
    
    std::cout<<"Number of grains: "<<nr_grains<<std::endl;
    return 0;
}

int calculate_grains(std::vector<Grains> &Grain, std::vector<Segment> &Segments, vigra::FVector3Image &vector_image, vigra::IImage &grain_label_image)
{
    std::cout << "calculate grain parameters.";
    for(int i=1; i<=nr_grains; i++)
    {
        Grain[i].axis[0]=Grain[i].axis[1]=Grain[i].axis[2]=0;
        Grain[i].az=0;
        Grain[i].co=0;
        Grain[i].r=0;
    }
    int grain_count = 0;
    int segment_count = 0;

    for(int i=1; i<=nr_regions; i++)
        if (Segments[i].phase == 1)
        {
            grain_count++;
            Segments[i].child_index = grain_count;
            Grain[grain_count].parent_index = i;
            Grain[grain_count].phase = Segments[i].phase;
            Grain[grain_count].size = Segments[i].size;
            Grain[grain_count].xcenter = Segments[i].xcenter;
            Grain[grain_count].ycenter = Segments[i].ycenter;
            Grain[grain_count].pointer = Segments[i].pointer;
        }

        
    //calculate mean axis vector    
    for(int i=1; i<=nr_grains; i++)
    {
        int x = 0;
        int y = 0;
        for(int pointer_iterator = 0; pointer_iterator < Grain[i].pointer.size(); pointer_iterator++)
        {
            x = Grain[i].pointer[pointer_iterator].x;
            y = Grain[i].pointer[pointer_iterator].y;
            if (vigra::dot(vector_image(x,y), Grain[i].axis)<0.0) Grain[i].axis-=vector_image(x,y);
            else Grain[i].axis+=vector_image(x,y);
        }
        
        Grain[i].r=(2*sqrt(vigra::squaredNorm(Grain[i].axis))/Grain[i].size-1.0)*100;
        Grain[i].axis=Grain[i].axis/sqrt(vigra::squaredNorm(Grain[i].axis));
    }
    
        
/*        
    //calculate center of mass positions, mean grain size and mean axis vector
    std::cout << ".";
    for (int x=0; x<mWidth; x++)
        for (int y=0; y<mHeight; y++)
        {
            //grain labeling starts with 1
            segment_count = grain_label_image(x,y);
            if (Segments[segment_count].phase == 1)
            {
                int i = grain_label_image(x,y);
                Grain[i].size++;
                Grain[i].xcenter+=x/float(mWidth);
                Grain[i].ycenter+=y/float(mHeight);
                if (vigra::dot(vector_image(x,y), Grain[i].axis)<0.0) Grain[i].axis-=vector_image(x,y);
                else Grain[i].axis+=vector_image(x,y);
                
                //if (bground_image(x,y)==0) Grain[i].phase=0;
            }
        }

    std::cout << ".";
    for(int i=1; i<=nr_grains; i++)
    {
        Grain[i].xcenter=Grain[i].xcenter/Grain[i].size*float(mWidth);
        Grain[i].ycenter=Grain[i].ycenter/Grain[i].size*float(mHeight);
        Grain[i].r=(2*sqrt(vigra::squaredNorm(Grain[i].axis))/Grain[i].size-1.0)*100;
        Grain[i].axis=Grain[i].axis/sqrt(vigra::squaredNorm(Grain[i].axis));
    }
*/
    // Transform to geo coordinates in horizontal projection
    std::cout << ".";
    for(int i=1; i<=nr_grains; i++)
    {
        if (Grain[i].axis[2]>0.0)
            Grain[i].axis=-Grain[i].axis;    //rotate all grain axis vectors in negative z-direction ("down") - projection onto the lower hemisphere
        if (Grain[i].axis[1]>0.0 && Grain[i].axis[0]>=0.0)
            Grain[i].az=atan(Grain[i].axis[0]/Grain[i].axis[1])*180/M_PI;
        else if (Grain[i].axis[1]<0.0)
            Grain[i].az=(atan(Grain[i].axis[0]/Grain[i].axis[1])+M_PI)*180/M_PI;
        else if (Grain[i].axis[1]>0.0 && Grain[i].axis[0]<0.0)
            Grain[i].az=(atan(Grain[i].axis[0]/Grain[i].axis[1])+2*M_PI)*180/M_PI;
        else if (Grain[i].axis[0]>0.0)   Grain[i].az=90;
        else if (Grain[i].axis[0]<0.0)   Grain[i].az=270;
        else    Grain[i].az=0;
        Grain[i].co=acos(-Grain[i].axis[2]/sqrt(vigra::squaredNorm(Grain[i].axis)))*180/M_PI; //acos(-z) because lower hemisphere projection 
    }
  
    std::cout << " done" << std::endl;
    return 0;
}

float get_length(std::vector<point> pixels)
{
     if (pixels.size()<2) return (float)pixels.size();

     bool horizontal_step=false;
     bool vertical_step=false;
     float length=0.0f;

     for (int i=1; i<pixels.size(); i++)
     {
         if (fabs(pixels[i].x-pixels[i-1].x)==1 && fabs(pixels[i].y-pixels[i-1].y)==0 && vertical_step)
         {
             length+=(sqrt(2)-1.0f);
             horizontal_step=false;
             vertical_step=false;
         }
         else if (fabs(pixels[i].x-pixels[i-1].x)==0 && fabs(pixels[i].y-pixels[i-1].y)==1 && horizontal_step)
         {
             length+=(sqrt(2)-1.0f);
             horizontal_step=false;
             vertical_step=false;
         }
         else
         {
            length+=sqrt((pixels[i].x-pixels[i-1].x)*(pixels[i].x-pixels[i-1].x)+
                 (pixels[i].y-pixels[i-1].y)*(pixels[i].y-pixels[i-1].y));

             if (fabs(pixels[i].x-pixels[i-1].x)==1 && fabs(pixels[i].y-pixels[i-1].y)==0)
                 horizontal_step=true;
             else
                horizontal_step=false;
             if (fabs(pixels[i].x-pixels[i-1].x)==0 && fabs(pixels[i].y-pixels[i-1].y)==1)
                 vertical_step=true;
             else
                 vertical_step=false;
         }
     }
     return length;
}

int calculate_boundaries(std::vector<Boundaries> &Boundary, std::vector<Grains> &Grain, std::vector<Segment> &Segments, std::string path)
{
    std::cout << "calculate boundary parameters..." << std::endl;
  
    //load seg structures
    seg segment(false);
    segment.load_obj_from_file(path.c_str());
    nr_boundaries = segment.arcs.size();
    int nr_junctions = segment.junctions.size();
    Boundary.resize(nr_boundaries+1);
   
    point junction;
    int boundary;
    for (int j=0; j<nr_junctions; j++)
        for (int i=0; i<=3; i++)
        {
            junction.x = segment.junctions[j].x;
            junction.y = segment.junctions[j].y;
            boundary = segment.one_boundings(j,i);
            if (boundary != 0)
                Boundary[boundary].junction.push_back(junction);
        }
//    std::cout << Boundary[500].junction.size();
    
    vigra::TinyVector<float, 3> xzplane;
    xzplane[0]=0.0; xzplane[1]=1.0; xzplane[2]=0.0;
    int grain1, grain2, segment_index1, segment_index2, xdist, ydist;
    double jjdistance;
    for (int i=1; i<=nr_boundaries; i++)
    {
        Boundary[i].junction.resize(2);
        Boundary[i].length = get_length(segment.arcs[i-1]);
        Boundary[i].pointer = segment.arcs[i-1];        //all points of the arc

        segment_index1 = segment.two_boundings(i-1,0);
        segment_index2 = segment.two_boundings(i-1,1);
        if (Segments[segment_index1].phase == 1)
            grain1 = Segments[segment_index1].child_index;
        else
            grain1 = 0;
        if (Segments[segment_index2].phase == 1)
            grain2 = Segments[segment_index2].child_index;
        else
            grain2 = 0;
        Boundary[i].grain.push_back(grain1);
        Boundary[i].grain.push_back(grain2);

        Boundary[i].misorientation = acos(fabs(vigra::dot(Grain[grain1].axis, Grain[grain2].axis)))*180/M_PI;

        xdist = abs(segment.arcs[i-1].front().x)-abs(segment.arcs[i-1].back().x);
        ydist = -abs(segment.arcs[i-1].front().y)+abs(segment.arcs[i-1].back().y);
    //    xdist = absBoundary[i].junction[0].x)-abs(Boundary[i].junction[1].x);
    //    ydist = -abs(Boundary[i].junction[0].y)+abs(Boundary[i].junction[1].y);
        Boundary[i].jjdistance = sqrt(pow(xdist, 2) + pow(ydist, 2));
        Boundary[i].jjratio = Boundary[i].jjdistance/Boundary[i].length;
        
        Boundary[i].xdist=xdist;
        Boundary[i].ydist=ydist;
        if (xdist == 0)
          Boundary[i].slope = 90.0f;
        else
          Boundary[i].slope = atan(float(ydist)/float(xdist))*180/M_PI;
        
        Boundary[i].crossproduct = vigra::cross(Grain[grain1].axis, Grain[grain2].axis);
        
        vigra::TinyVector<float, 3> axis_sum;
        if (vigra::dot(Grain[grain1].axis, Grain[grain2].axis) >= 0.0)
          axis_sum = Grain[grain1].axis+Grain[grain2].axis;
        else
          axis_sum = Grain[grain1].axis-Grain[grain2].axis;
        vigra::TinyVector<float, 3> tilt = vigra::cross(Boundary[i].crossproduct, axis_sum);
        vigra::TinyVector<float, 2> tiltprojection, boundaryprojection, twistprojection;
        tiltprojection[0]=-tilt[2];
        tiltprojection[1]=tilt[0];
        tiltprojection = tiltprojection/sqrt(vigra::squaredNorm(tiltprojection));
        boundaryprojection[0]=float(xdist);
        boundaryprojection[1]=float(ydist);
        boundaryprojection = boundaryprojection/sqrt(vigra::squaredNorm(boundaryprojection));
        twistprojection[0]=-Boundary[i].crossproduct[2];
        twistprojection[1]=Boundary[i].crossproduct[0];
        twistprojection = twistprojection/sqrt(vigra::squaredNorm(twistprojection));
        Boundary[i].tilt_to_boundary = acos(fabs(vigra::dot(tiltprojection, boundaryprojection)))*180/M_PI;
        Boundary[i].twist_to_boundary = acos(fabs(vigra::dot(twistprojection, boundaryprojection)))*180/M_PI;
        
        if (Boundary[i].crossproduct[0] == 0.0f)
            Boundary[i].cross_slope = 90.0f;
        else
            Boundary[i].cross_slope = atan(Boundary[i].crossproduct[2]/Boundary[i].crossproduct[0])*180/M_PI;
        
        // Transform to geo coordinates in horizontal projection
        if (Boundary[i].crossproduct[2]>0.0)
            Boundary[i].crossproduct=-Boundary[i].crossproduct;    //rotate all grain axis vectors in negative z-direction ("down")
        if (Boundary[i].crossproduct[1]>0.0 && Boundary[i].crossproduct[0]>=0.0)
            Boundary[i].cross_az=atan(Boundary[i].crossproduct[0]/Boundary[i].crossproduct[1])*180/M_PI;
        else if (Boundary[i].crossproduct[1]<0.0)
            Boundary[i].cross_az=(atan(Boundary[i].crossproduct[0]/Boundary[i].crossproduct[1])+M_PI)*180/M_PI;
        else if (Boundary[i].crossproduct[1]>0.0 && Boundary[i].crossproduct[0]<0.0)
            Boundary[i].cross_az=(atan(Boundary[i].crossproduct[0]/Boundary[i].crossproduct[1])+2*M_PI)*180/M_PI;
        else if (Boundary[i].crossproduct[0]>0.0)   Boundary[i].cross_az=90;
        else if (Boundary[i].crossproduct[0]<0.0)   Boundary[i].cross_az=270;
        else    Boundary[i].cross_az=0;
        Boundary[i].cross_co=acos(-Boundary[i].crossproduct[2]/sqrt(vigra::squaredNorm(Boundary[i].crossproduct)))*180/M_PI; //acos(-z) because lower hemisphere projection
    }
    
    return 0;
}

int calculate_statistics(vigra::IImage &grain_label_image, vigra::FVector3Image &vector_image, std::vector<Grains> &Grain, vigra::BImage bground_image)
{
    //General Statistics
    std::cout << "calculate c-axes statistics.";
    vigra::TinyVector<float, 3> grain_sum_vector;
    mean_size = 0;
    for(int i=1; i<=nr_grains; i++)
    {
        if (vigra::dot(grain_sum_vector, Grain[i].axis)<0.0) grain_sum_vector-=Grain[i].axis;
        else grain_sum_vector+=Grain[i].axis;
        mean_size += Grain[i].size;
    }
    float grain_sum_vector_norm = sqrt(vigra::squaredNorm(grain_sum_vector));
    mean_size = ceil(mean_size/nr_grains);

    // area weighted
    std::cout << ".";
    int nr_points = 0;
    vigra::TinyVector<double, 3> sum_vector;
    sum_vector[0] = 0.0; sum_vector[1] = 0.0; sum_vector[2] = 0.0;
    for (int x=0; x<mWidth; x++)
        for (int y=0; y<mHeight; y++)
        {
            if (grain_label_image(x,y)>0 && bground_image(x,y)==1)
            {
                nr_points++;
                if (vigra::dot(vector_image(x,y), sum_vector)<0.0) sum_vector-=vector_image(x,y);
                else sum_vector+=vector_image(x,y);
            }
        }
    sum_vector_norm = sqrt(vigra::squaredNorm(sum_vector));
    regelungsgrad = (2*sum_vector_norm-nr_points)/nr_points*100;
    concentration_parameter = (nr_points-1)/(nr_points-sum_vector_norm);
    spherical_aperture = asin(sqrt(2*(1-1/nr_points)/concentration_parameter))*180/M_PI;
    sum_vector_norm = sum_vector_norm/nr_points;
    sum_vector = sum_vector/nr_points;
    vigra::linalg::Matrix<double> orientation_tensor(3,3), eigenvectors(3,3), eigenvalues(3,1);
    vigra::TinyVector<float, 3> point;
    for (int x=0; x<mWidth; x++)
        for (int y=0; y<mHeight; y++)
        {
            if (grain_label_image(x,y)>0 && bground_image(x,y)==1)
            {
                if (vector_image(x,y)[2]<0.0) point = -vector_image(x,y);
                else    point = vector_image(x,y);
                orientation_tensor(0,0)+=point[0]*point[0];
                orientation_tensor(1,1)+=point[1]*point[1];
                orientation_tensor(2,2)+=point[2]*point[2];
                orientation_tensor(0,1)=orientation_tensor(1,0)+=point[0]*point[1];
                orientation_tensor(0,2)=orientation_tensor(2,0)+=point[0]*point[2];
                orientation_tensor(2,1)=orientation_tensor(1,2)+=point[2]*point[1];
            }
        }
    vigra::linalg::symmetricEigensystem(orientation_tensor, eigenvalues, eigenvectors);

    e1 = eigenvalues(2)/nr_points;
    e2 = eigenvalues(1)/nr_points;
    e3 = eigenvalues(0)/nr_points;
    
    // eigenvectors area weighted
    
    for (int i=0; i<=2; i++) // read out matrix and convert eigenvectors in tinyvectors
    {
        eigenvector_a[0][i] = eigenvectors(i,2);
        eigenvector_a[1][i] = eigenvectors(i,1);
        eigenvector_a[2][i] = eigenvectors(i,0);
    }
    
 
    // Transform to geo coordinates in horizontal projection

    for (int e=0; e<3; e++)
    {
        if (eigenvector_a[e][2]>0.0)
            eigenvector_a[e]=-eigenvector_a[e];    //rotate all vectors in negative z-direction ("down") - projection onto the lower hemisphere
        if (eigenvector_a[e][1]>0.0 && eigenvector_a[e][0]>=0.0)
            eigenv_a_az[e]=atan(eigenvector_a[e][0]/eigenvector_a[e][1])*180/M_PI;
        else if (eigenvector_a[e][1]<0.0)
            eigenv_a_az[e]=(atan(eigenvector_a[e][0]/eigenvector_a[e][1])+M_PI)*180/M_PI;
        else if (eigenvector_a[e][1]>0.0 && eigenvector_a[e][0]<0.0)
            eigenv_a_az[e]=(atan(eigenvector_a[e][0]/eigenvector_a[e][1])+2*M_PI)*180/M_PI;
        else if (eigenvector_a[e][0]>0.0)   eigenv_a_az[e]=90;
        else if (eigenvector_a[e][0]<0.0)   eigenv_a_az[e]=270;
        else    eigenv_a_az[e]=0;
        eigenv_a_co[e]=acos(-eigenvector_a[e][2]/sqrt(vigra::squaredNorm(eigenvector_a[e])))*180/M_PI; //acos(-z) because lower hemisphere projection
    }
            
    std::cout << std::endl;
    
    for (int e=0; e<3; e++)
        std::cout << eigenv_a_az[e] << "\t" << eigenv_a_co[e] << std::endl;
    
    woodcock = log(e3/e2)/log(e2/e1);
    
    // grain weighted
    std::cout << ".";
    regelungsgrad_g = (2*grain_sum_vector_norm-nr_grains)/nr_grains*100;
    concentration_parameter_g = (nr_grains-1)/(nr_grains-grain_sum_vector_norm);
    spherical_aperture_g = asin(sqrt(2*(1-1/nr_grains)/concentration_parameter_g))*180/M_PI;
    orientation_tensor(0,0)=orientation_tensor(0,1)=orientation_tensor(0,2)=orientation_tensor(1,0)=orientation_tensor(1,1)=orientation_tensor(1,2)=orientation_tensor(2,0)=orientation_tensor(2,1)=orientation_tensor(2,2)=0.0;
    
    for (int i=1; i<=nr_grains; i++)
    {
        if (Grain[i].phase==1)
        {
        if (Grain[i].axis[2]<0.0) point = -Grain[i].axis;
        else    point = Grain[i].axis;
        orientation_tensor(0,0)+=point[0]*point[0];
        orientation_tensor(1,1)+=point[1]*point[1];
        orientation_tensor(2,2)+=point[2]*point[2];
        orientation_tensor(0,1)=orientation_tensor(1,0)+=point[0]*point[1];
        orientation_tensor(0,2)=orientation_tensor(2,0)+=point[0]*point[2];
        orientation_tensor(2,1)=orientation_tensor(1,2)+=point[2]*point[1];
        }
    }
    vigra::linalg::symmetricEigensystem(orientation_tensor, eigenvalues, eigenvectors);
    
    e1_g = eigenvalues(2,0)/nr_grains;
    e2_g = eigenvalues(1,0)/nr_grains;
    e3_g = eigenvalues(0,0)/nr_grains;
    
    // eigenvectors grain weighted
    
    for (int i=0; i<=2; i++) // read out matrix and convert eigenvectors in tinyvectors
    {
        eigenvector_g[0][i] = eigenvectors(i,2);
        eigenvector_g[1][i] = eigenvectors(i,1);
        eigenvector_g[2][i] = eigenvectors(i,0);
    }
    
 
    // Transform to geo coordinates in horizontal projection

    for (int e=0; e<3; e++)
    {
        if (eigenvector_g[e][2]>0.0)
            eigenvector_g[e]=-eigenvector_g[e];    //rotate all vectors in negative z-direction ("down") - projection onto the lower hemisphere
        if (eigenvector_g[e][1]>0.0 && eigenvector_g[e][0]>=0.0)
            eigenv_g_az[e]=atan(eigenvector_g[e][0]/eigenvector_g[e][1])*180/M_PI;
        else if (eigenvector_g[e][1]<0.0)
            eigenv_g_az[e]=(atan(eigenvector_g[e][0]/eigenvector_g[e][1])+M_PI)*180/M_PI;
        else if (eigenvector_g[e][1]>0.0 && eigenvector_g[e][0]<0.0)
            eigenv_g_az[e]=(atan(eigenvector_g[e][0]/eigenvector_g[e][1])+2*M_PI)*180/M_PI;
        else if (eigenvector_g[e][0]>0.0)   eigenv_g_az[e]=90;
        else if (eigenvector_g[e][0]<0.0)   eigenv_g_az[e]=270;
        else    eigenv_g_az[e]=0;
        eigenv_g_co[e]=acos(-eigenvector_g[e][2]/sqrt(vigra::squaredNorm(eigenvector_g[e])))*180/M_PI; //acos(-z) because lower hemisphere projection
    }
            
    std::cout << std::endl;
    
    for (int e=0; e<3; e++)
        std::cout << eigenv_g_az[e] << "\t" << eigenv_g_co[e] << std::endl;
    
    woodcock_g = log(e3_g/e2_g)/log(e2_g/e1_g);
    
    std::cout << " done" << std::endl;
    return 0;
}

int write_general_results (std::string path)
{
    //write output file
    std::ofstream output_file(path.c_str());
    
    std::time_t now = std::time(0);
    char* time_now = std::ctime(&now);
    
    output_file << "# ++ DATA INFO ++\n"
    << "# instrument = " << instrument << "\n"
    << "# section folder = " << sourcepath << "\n"
    << "# date = " << date << "\n"
    << "# mineral = " << mineral << "\n"
    << "# image width = " << mWidth << " pixels\n"
    << "# image height = " << mHeight << " pixels\n"
    << "# pixelsize = " << pixelsize << "um square\n"
    << "# tile size = " << scanned_height/tiles_high << "mm square\n"
    << "# tiles wide = " << tiles_wide << "\n"
    << "# tiles high = " << tiles_high << "\n"
    << "# scanned width = " << scanned_width << "mm\n"
    << "# scanned height = " << scanned_height << "mm\n";
    
    output_file << "\n# ++ EVALUATION INFO ++\n"
    << "# data processed: " << time_now
    << "# cAxes version = " << cAxes_version << "\n"
    << "# minimal geometric quality [%] = " << gqmin << "\n"
    << "# minimal retardation quality [%] = " << rqmin << "\n"
    << "# misorientation threshold [degree] = " << maximal_misorientation << "\n"
    << "# minimal grain size [pixel] = " << minimal_grain_size << "\n"
    << "# tile rotation angle [degree] = " << rot_angle << "\n"
    << "# section geometry (0 horizontal, 1 vertical) = " << vertical_cut << "\n"
    << "# horizontal orientation angle [degree] " << rotate_north << "\n"
    
    << "\n# ++ GRAIN STATISTICS ++\n"
    << "# nr of grains = " << nr_grains << "\n"
    << "# mean grain size [pixel] = " << mean_size << "\n"
    
    << "\n# ++ C-AXIS ORIENTATION STATISTICS ++\n"
    << "# sum vector norm = " << sum_vector_norm << "\n"
    
    << "\n# weighted by grain area: \n"
    << "# regelungsgrad = " << regelungsgrad << "\n"
    << "# concentration parameter = " << concentration_parameter << "\n"
    << "# spherical aperture [degree] = " << spherical_aperture << "\n"
    << "# eigenvalues (e1, e2, e3) = (" << e1 << ", " << e2 << ", " << e3 << ")\n"
    << "# eigenvector_1 (azimuth, latitude, colatitude) [degree] = (" << eigenv_a_az[0] << ", " << 90-eigenv_a_co[0] << ", " << eigenv_a_co[0] << ")\n"
    << "# eigenvector_2 (azimuth, latitude, colatitude) [degree] = (" << eigenv_a_az[1] << ", " << 90-eigenv_a_co[1] << ", " << eigenv_a_co[1] << ")\n"
    << "# eigenvector_3 (azimuth, latitude, colatitude) [degree] = (" << eigenv_a_az[2] << ", " << 90-eigenv_a_co[2] << ", " << eigenv_a_co[2] << ")\n"
    << "# woodcock parameter = " << woodcock << "\n"
    
    << "\n# not weighted (per grain): \n"
    << "# regelungsgrad = " << regelungsgrad_g << "\n"
    << "# concentration parameter = " << concentration_parameter_g << "\n"
    << "# spherical aperture [degree] = " << spherical_aperture_g << "\n"
    << "# eigenvalues (e1, e2, e3) = (" << e1_g << ", " << e2_g << ", " << e3_g << ")\n"
    << "# eigenvector_1 (azimuth, latitude, colatitude) [degree] = (" << eigenv_g_az[0] << ", " << 90-eigenv_g_co[0] << ", " << eigenv_g_co[0] << ")\n"
    << "# eigenvector_2 (azimuth, latitude, colatitude) [degree] = (" << eigenv_g_az[1] << ", " << 90-eigenv_g_co[1] << ", " << eigenv_g_co[1] << ")\n"
    << "# eigenvector_3 (azimuth, latitude, colatitude) [degree] = (" << eigenv_g_az[2] << ", " << 90-eigenv_g_co[2] << ", " << eigenv_g_co[2] << ")\n"
    << "# woodcock parameter = " << woodcock_g << "\n";
    
    output_file.close();
  
    return 0;
}

int write_grains_txt (std::vector<Grains> &Grain, std::string path)
{
    //write output file
    std::ofstream output_file(path.c_str());
    
    output_file << "# ++ GRAIN PROPERTIES ++\n"
    << "# c1 grain number\n"
    << "# c2 size [pixel]\n"
    << "# c3 center x-coordinate\n"
    << "# c4 center y-coordinate\n"
    << "# c5 mean azimuth [degree]\n"
    << "# c6 mean latitude [degree]\n"
    << "# c7 mean colatitude [degree]\n"
    << "# c8 regelungsgrad [%]\n"
    << "# c9 phase (0 background, 1 ice)\n";

    for(int i=1; i<=nr_grains; i++)
    {
        output_file << i << "\t"
            << Grain[i].size << "\t"
            << ceil(Grain[i].xcenter) << "\t" 
            << ceil(Grain[i].ycenter) << "\t"
            << Grain[i].az << "\t"
            << 90-Grain[i].co << "\t"
            << Grain[i].co << "\t"
            << Grain[i].r << "\t"
            << Grain[i].phase << "\n";
    }

    output_file.close();
  
    return 0;
}

int write_grains_txt2 (std::vector<Grains> &Grain, std::string path)
{
    //write output file
    std::ofstream output_file(path.c_str());
    
    output_file << "# ++ DATA INFO ++\n"
    << "# instrument = " << instrument << "\n"
    << "# section folder = " << sourcepath << "\n"
    << "# date = " << date << "\n"
    << "# mineral = " << mineral << "\n"
    << "# image width = " << mWidth << " pixels\n"
    << "# image height = " << mHeight << " pixels\n"
    << "# pixelsize = " << pixelsize << "um square\n"
    << "# tile size = " << scanned_height/tiles_high << "mm square\n"
    << "# tiles wide = " << tiles_wide << "\n"
    << "# tiles high = " << tiles_high << "\n"
    << "# scanned width = " << scanned_width << "mm\n"
    << "# scanned height = " << scanned_height << "mm\n";
    
    output_file << "\n# ++ EVALUATION INFO ++\n"
    << "# minimal geometric quality [%] = " << gqmin << "\n"
    << "# minimal retardation quality [%] = " << rqmin << "\n"
    << "# misorientation threshold [degree] = " << maximal_misorientation << "\n"
    << "# minimal grain size [pixel] = " << minimal_grain_size << "\n"
    << "# tile rotation angle [degree] = " << rot_angle << "\n"
    
    << "\n# ++ GENERAL C-AXIS STATISTICS ++\n"
    << "# nr of grains = " << nr_grains << "\n"
    << "# mean grain size [pixel] = " << mean_size << "\n"
    << "# sum vector norm = " << sum_vector_norm << "\n"
    << "# regelungsgrad = " << regelungsgrad << "\n"
    << "# concentration parameter = " << concentration_parameter << "\n"
    << "# spherical aperture [degree]= " << spherical_aperture << "\n"
    << "# eigenvalue e1 = " << e1 << "\n"
    << "# eigenvalue e2 = " << e2 << "\n"
    << "# eigenvalue e3 = " << e3 << "\n"
    << "# woodcock parameter = " << woodcock << "\n"
    
    << "\n# ++ GRAIN PROPERTIES ++\n"
    << "# c1 grain number\n"
    << "# c2 size [pixel]\n"
    << "# c3 center x-coordinate\n"
    << "# c4 center y-coordinate\n"
    << "# c5 axis 0 [degree]\n"
    << "# c6 axis 1 [degree]\n"
    << "# c7 axis 2 [degree]\n"
    << "# c8 mean azimuth [degree]\n"
    << "# c9 mean colatitude [degree]\n"
    << "# c10 regelungsgrad [%]\n";

    for(int i=1; i<=nr_grains; i++)
    {
        output_file << i << "\t"
            << Grain[i].size << "\t"
            << ceil(Grain[i].xcenter) << "\t" 
            << ceil(Grain[i].ycenter) << "\t"
            << Grain[i].axis[0] << "\t"
            << Grain[i].axis[1] << "\t"
            << Grain[i].axis[2] << "\t"
            << Grain[i].az << "\t"
            << Grain[i].co << "\t"
            << Grain[i].r << "\n";
    }

    output_file.close();
  
    return 0;
}

int write_stereo_txt (std::vector<Grains> &Grain, std::string path)
{
    //write stereo file
    std::ofstream stereo_file(path.c_str());
    stereo_file << "# azimuth" << "\t" << "latitude" << "\n";
    for(int i=1; i<=nr_grains; i++)
    {
        stereo_file << Grain[i].az << "\t"
            << 90-Grain[i].co << "\n"; //latitude!
    }
    stereo_file.close();
    
    return 0;
}

int write_xstereo_txt (std::vector<Boundaries> &Boundary, std::string path)
{
    //write stereo file
    std::ofstream stereo_file(path.c_str());
    stereo_file << "# azimuth" << "\t" << "latitude" << "\n";
    for(int i=1; i<=nr_boundaries; i++)
        if (Boundary[i].cross_az != 0)
        {
            stereo_file << Boundary[i].cross_az << "\t"
                << 90-Boundary[i].cross_co << "\n"; //latitude!
        }
    stereo_file.close();
    
    return 0;
}

int write_boundaries_txt (std::vector<Boundaries> &Boundary, std::string path)
{
    //write output file
    std::ofstream output_file(path.c_str());
    
    output_file << "# ++ BOUNDARY PROPERTIES ++\n"
    << "# c1 boundary number\n"
    << "# c2 grain1\n"
    << "# c3 grain2\n"
    << "# c4 length [pixel]\n"
    << "# c5 misorientation angle [degree]\n"
    << "# c6 jjdistance-to-length ratio\n"
    << "# c7 slope angle\n"
    << "# c8 tilt to boundary angle\n"
    << "# c9 twist to boundary angle\n"
    << "# c10 rotation vector az\n";

    for(int i=1; i<=nr_boundaries; i++)
    {
        output_file << i << "\t"
            << Boundary[i].grain[0] << "\t"
            << Boundary[i].grain[1] << "\t"
            << Boundary[i].length << "\t"
            << Boundary[i].misorientation << "\t"
            << Boundary[i].jjratio << "\t"
            << Boundary[i].slope << "\t"
            << Boundary[i].tilt_to_boundary << "\t"
            << Boundary[i].twist_to_boundary << "\t"
            << Boundary[i].cross_az << "\n";
    }

    output_file.close();
  
    return 0;
}
