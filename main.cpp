#include "caxes.h"
#include "misorient.cpp"
#include <unistd.h>



//functions
int analysis();

int main(int argc, char ** argv)
{
    if (argc==1) //command line modus
    {
	//hello
	std::cout << "cAxes is a program for the processing of the Fabric Analyser data. \n";
	std::cout << "Version: " << cAxes_version << "\n\n";
      
        //input parameters
        std::cout << "Enter minimal geometric quality: ";
        std::cin >> gqmin;
        std::cout << "Enter minimal retardation quality: ";
        std::cin >> rqmin;
        std::cout << "Enter misorientation threshold in degrees: ";
        std::cin >> maximal_misorientation;    
        std::cout << "Enter minimal grain size in pixel: ";
        std::cin >> minimal_grain_size;
        std::cout << "Enter tiles rotation angle in degrees: ";
        std::cin >> rot_angle;
        std::cout << "Enter section type (0 = horizontal; 1 = vertical): ";
        std::cin >> vertical_cut;
        std::cout << "Rotate North in degrees: ";
        std::cin >> rotate_north;
        //std::cout << "Eliminate tile borders? (0 = no; 1 = yes): ";
        //std::cin >> eliminate_borders;    
        eliminate_borders = false;


        // Read pathlisting.txt
        std::ifstream pathlisting ("pathlisting.txt");
        if (pathlisting.is_open())
        {
            std::ofstream statistics("statistics.txt");
            
            std::time_t now = std::time(0);
            char* time_now = std::ctime(&now);
            
            statistics << "# ++ EVALUATION INFO ++\n"
                << "# data processed: " << time_now
                << "# cAxes version = " << cAxes_version << "\n"
                << "# minimal geometric quality [%] = " << gqmin << "\n"
                << "# minimal retardation quality [%] = " << rqmin << "\n"
                << "# misorientation threshold [degree] = " << maximal_misorientation << "\n"
                << "# minimal grain size [pixel] = " << minimal_grain_size << "\n"
                << "# tile rotation angle [degree] = " << rot_angle << "\n"
                << "# section geometry (0 horizontal, 1 vertical) = " << vertical_cut << "\n"
                << "# horizontal orientation angle [degree] " << rotate_north << "\n"
                
                << "\n# ++ COLUMN HEADERS ++\n"
                << "# c1 data directory\n"
                << "# c2 nr of grains\n"
                << "# c3 mean grain size [pixel]\n"
                << "# c4 sum vector norm\n"
                
                << "### weighted by grain area:\n"
                << "# c5 regelungsgrad\n"
                << "# c6 concentration_parameter\n"
                << "# c7 spherical_aperture [degree]\n"
                << "# c8 eigenvalue e1\n"
                << "# c9 eigenvalue e2\n"
                << "# c10 eigenvalue e3\n"
                << "# c11 eigenvector_1 azimuth [degree]\n"
                << "# c12 eigenvector_1 latitude [degree]\n"
                << "# c13 eigenvector_1 colatitude [degree]\n"
                << "# c14 eigenvector_2 azimuth [degree]\n"
                << "# c15 eigenvector_2 latitude [degree]\n"
                << "# c16 eigenvector_2 colatitude [degree]\n"
                << "# c17 eigenvector_3 azimuth [degree]\n"
                << "# c18 eigenvector_3 latitude [degree]\n"
                << "# c19 eigenvector_3 colatitude [degree]\n"
                << "# c20 woodcock parameter\n"
                
                << "### not weighted (per grain):\n"
                << "# c21 regelungsgrad\n"
                << "# c22 concentration_parameter\n"
                << "# c23 spherical_aperture [degree]\n"
                << "# c24 eigenvalue e1\n"
                << "# c25 eigenvalue e2\n"
                << "# c26 eigenvalue e3\n"
                << "# c27 eigenvector_1 azimuth [degree]\n"
                << "# c28 eigenvector_1 latitude [degree]\n"
                << "# c29 eigenvector_1 colatitude [degree]\n"
                << "# c30 eigenvector_2 azimuth [degree]\n"
                << "# c31 eigenvector_2 latitude [degree]\n"
                << "# c32 eigenvector_2 colatitude [degree]\n"
                << "# c33 eigenvector_3 azimuth [degree]\n"
                << "# c34 eigenvector_3 latitude [degree]\n"
                << "# c35 eigenvector_3 colatitude [degree]\n"
                << "# c36 woodcock parameter\n"
                
                << "\n# ++ DATA ++\n";
                
            statistics.close();
            
            while (getline(pathlisting, sourcepath))
            {
                int status = analysis();
                statistics.open("statistics.txt", std::ios::app);
                
                if (status == 0)
                {
                    statistics << sourcepath << "\t"
                        << nr_grains << "\t"
                        << mean_size << "\t"
                        << sum_vector_norm << "\t" 
                        << regelungsgrad << "\t"
                        << concentration_parameter << "\t"
                        << spherical_aperture << "\t"
                        << e1 << "\t"
                        << e2 << "\t"
                        << e3 << "\t"
                        << eigenv_a_az[0] << "\t" << 90-eigenv_a_co[0] << "\t" << eigenv_a_co[0] << "\t"
                        << eigenv_a_az[1] << "\t" << 90-eigenv_a_co[1] << "\t" << eigenv_a_co[1] << "\t"
                        << eigenv_a_az[2] << "\t" << 90-eigenv_a_co[2] << "\t" << eigenv_a_co[2] << "\t"
                        << woodcock << "\t"
                        << regelungsgrad_g << "\t"
                        << concentration_parameter_g << "\t"
                        << spherical_aperture_g << "\t"
                        << e1_g << "\t"
                        << e2_g << "\t"
                        << e3_g << "\t"
                        << eigenv_g_az[0] << "\t" << 90-eigenv_g_co[0] << "\t" << eigenv_g_co[0] << "\t"
                        << eigenv_g_az[1] << "\t" << 90-eigenv_g_co[1] << "\t" << eigenv_g_co[1] << "\t"
                        << eigenv_g_az[2] << "\t" << 90-eigenv_g_co[2] << "\t" << eigenv_g_co[2] << "\t"
                        << woodcock_g << "\n";
                }
                else statistics << sourcepath << " error status " << status << "\n";
                
                statistics.close();
            }
            pathlisting.close();
        }
        else
        {
            std::cout << "Enter path to data.cis folder: ";
            std::cin >> sourcepath;
            
            analysis();
        }
    }
    
    else //GUI modus
    {
        std::string command_line_option="";
        command_line_option=argv[1];

        if (command_line_option=="-misorientation")
        {
            float plotmax;
        
            if(argc < 4)
            {
                std::cout << "Enter path to data.cis folder: ";
                std::cin >> sourcepath;
                std::cout << "Enter the misorientation-scale maximum: ";
                std::cin >> plotmax;
                vertical_cut = true;
            }
            else
            {
                sourcepath = argv[2];
                sourcepath = sourcepath.substr(0, sourcepath.length() -1);
                plotmax = atof(argv[3]);
                vertical_cut = atoi(argv[4]);
            }

            misorientation(plotmax);
        }
        else
        {
            //input parameters
            sourcepath = argv[1];
            sourcepath = sourcepath.substr(0, sourcepath.length() -1);
            gqmin = atoi(argv[2]);
            rqmin = atoi(argv[3]);
            maximal_misorientation = atof(argv[4]);
            minimal_grain_size = atoi(argv[5]);
            rot_angle = atof(argv[6]);
            vertical_cut = atoi(argv[7]);
            
            std::cout << "gquality = " << gqmin << std::endl;
            std::cout << "rquality = " << rqmin << std::endl;
            std::cout << "misorientation = " << maximal_misorientation << std::endl;
            std::cout << "minsize = " << minimal_grain_size << std::endl;
            std::cout << "tilerotation = " << rot_angle << std::endl;
            std::cout << "vertical_cut = " << vertical_cut << std::endl;

            eliminate_borders = false;
            bground_mask_found = false;

            analysis();
        }
    }

    return 0;
}

int analysis()
{
    //Filepath settings
    std::string new_directory = sourcepath+"/cAxes";
    mkdir(new_directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    std::string filepath_info = sourcepath+"/info.txt";
    std::string filepath_binary = sourcepath+"/data.cis";
    std::string filepath_bground_mask = sourcepath+"/background.bmp";
    std::string filepath_h5file = new_directory+"/regions.h5";
    std::string filepath_objects = new_directory+"/regions.objects.h5";
    std::string filepath_cgp = new_directory+"/regions";
    std::string filepath_grains_bw = new_directory+"/grains_bw.png";
    std::string filepath_grains_rgb = new_directory+"/grains_rgb.png";
    std::string filepath_grains_trendw = new_directory+"/grains_trendw.png";
    std::string filepath_vectors_rgb = new_directory+"/vectorimage_rgb.png";
    std::string filepath_boundary_rgb = new_directory+"/boundary_rgb.png";
    std::string filepath_boundary_trendw = new_directory+"/boundary_trendw.png";
    std::string filepath_regions = new_directory+"/regions.bmp";
    std::string filepath_general_txt = new_directory+"/general_results.txt";
    std::string filepath_grains_txt = new_directory+"/grains.txt";
    std::string filepath_grains2_txt = new_directory+"/grains2.txt";
    std::string filepath_boundaries_txt = new_directory+"/boundaries.txt";
    std::string filepath_stereo = new_directory+"/stereo.txt";
    std::string filepath_xstereo = new_directory+"/xstereo.txt";
    std::string filepath_quality = new_directory+"/quality.png";
    std::string filepath_topo_image = new_directory+"/subgrain_structure.png";     
    
    
    /*********************************************************/
    /* Import data and create vector_image and quality_image */
    /*********************************************************/
    
    std::cout << "\n";

    int import_status;
    import_status = import_data_g50AWI();
    if (import_status != 0)
        import_status = import_data_g60AWI();
    if (import_status != 0)
        import_status = import_data_g60Mainz();
    if (import_status != 0)
        import_status = import_data_g50LGGE();
    if (import_status != 0)
    {
        std::cout << "ERROR: no data found in " << sourcepath << std::endl;
        return import_status;
    }

    // Rotate North in vector_image
    if (rotate_north != 0)
    {
      rotate_north_vector_image(vector_image);
//      rotate_north_vector_image(vector_image2);
    }
      
    // Create bground_image
    vigra::BImage bground_image(mWidth,mHeight);  //0=background, 1=sample
    if (file_exists(filepath_bground_mask))
    {
        std::cout << "background-mask image found" << std::endl;
        create_bground(bground_image, filepath_bground_mask);
        bground_mask_found = true;
    }
    else
    {
        bground_image = 1;
        bground_mask_found = false;
    }
      
    // Create topo_image
    vigra::FImage topo_image(mWidth, mHeight);
    create_topo(vector_image, topo_image);
    
    // erase bad quality and background pixels from topo_image - set their values to 100
    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
            if(quality_image(x,y)==0 || bground_image(x,y)==0)
	      topo_image(x,y)=100.0;

    // Assign tile borders
    int nr_tile_borders = tiles_high*mWidth+tiles_wide*mHeight;
    int tile_borders[nr_tile_borders][2];
    borderpixel = 0;
    
    for (int xtile=1; xtile<tiles_wide; xtile++)    //assign left tile borders (first column)
        for (int y=1; y<mHeight; y++)
        {
            tile_borders[borderpixel][0]=tile_size*xtile;
            tile_borders[borderpixel][1]=y;
            borderpixel++;
        }
    for (int ytile=1; ytile<tiles_high; ytile++)    //assign upper tile borders (first row)
        for (int x=1; x<mWidth; x++)
        {
            tile_borders[borderpixel][0]=x;
            tile_borders[borderpixel][1]=tile_size*ytile;
            borderpixel++;
        }
    
    //Offset tiles, only for offset-version correcting data with defect FA-motor-axis. Usually disabled.
    /*
    offset_topo_image(topo_image);
    offset_vector_image(vector_image, tile_borders);
    offset_vector_image(vector_image2, tile_borders);
    offset_quality_image(quality_image);
    if (bground_mask_found)
      offset_quality_image(bground_image);
    */
    
    //Rotate tiles
    if (rot_angle != 0.0)
    {
        rotate_topo_image(topo_image);
        rotate_vector_image(vector_image, tile_borders);
//        rotate_vector_image(vector_image2, tile_borders);
        rotate_boolean_image(quality_image);
        if (bground_mask_found)
            rotate_boolean_image(bground_image);
    }

    /*
    exportImage(srcImageRange(az_image), vigra::ImageExportInfo("az.bmp"));
    exportImage(srcImageRange(co_image), vigra::ImageExportInfo("co.bmp"));
    exportImage(srcImageRange(nm_image), vigra::ImageExportInfo("nm.bmp"));
    exportImage(srcImageRange(rq_image), vigra::ImageExportInfo("rq.bmp"));
    exportImage(srcImageRange(gq_image), vigra::ImageExportInfo("gq.bmp"));
    exportImage(srcImageRange(quality_image), vigra::ImageExportInfo("quality.bmp"));
    exportImage(srcImageRange(bground_image), vigra::ImageExportInfo("bground.bmp"));*/
    
    
/*
    vigra::BasicImage<bool> topo_binary_image(mWidth,mHeight);

    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {
            if(topo_image(x,y)>maximal_misorientation) topo_binary_image(x,y)=true;
            else topo_binary_image(x,y)=false;
        }

    vigra::IImage topo_label_image(mWidth,mHeight);
    int nr_topo_labels = vigra::labelImageWithBackground(vigra::srcImageRange(topo_binary_image), vigra::destImage(topo_label_image), false, 0);
    std::vector<std::vector<point> > topo_regions(nr_topo_labels);

    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
        {
            if(topo_label_image(x,y)>0)
            {
                point p;
                p.x=x;
                p.y=y;
                topo_regions[topo_label_image(x,y)-1].push_back(p);
                topo_binary_image(x,y)=false;
            }
            else if (quality_image(x,y)>0) topo_binary_image(x,y)=true;
        }

    for (int label=0; label<nr_topo_labels; label++)
        if(topo_regions[label].size()>10 && topo_regions[label].size()<1000)
        {
            for (int p=0; p<topo_regions[label].size(); p++)
                topo_binary_image(topo_regions[label][p].x,topo_regions[label][p].y)=true;
        }
*/
//    exportImage(srcImageRange(topo_binary_image), vigra::ImageExportInfo(filepath_quality.c_str()));
    exportImage(srcImageRange(quality_image), vigra::ImageExportInfo(filepath_quality.c_str()));
    
    /*********************************************/
    /* Segmentation of vector image              */
    /* create grain_label_image                  */
    /*********************************************/
    
    // Find segments with lower than maximal_misorientation
    std::cout << "Processing image segmentation..." << std::endl;

    vigra::UInt32Image label_image(mWidth,mHeight);


    nr_regions = vigra::generateWatershedSeeds(srcImageRange(topo_image), destImage(label_image),
                                      vigra::FourNeighborCode(),
                                      vigra::SeedOptions().levelSets().threshold(maximal_misorientation));
   
    // filter low quality pixels
    filter_low_quality(label_image, quality_image);
    std::cout << "Number of marked segments: " << nr_regions << std::endl;
    
    // filter small grains
//    filter_small_grains(label_image);

    // if no bground_mask found create bground_image using cut_margin function
    if (bground_mask_found == false)
        cut_margin(label_image, bground_image);
    
    // assign bground areas in label_image
    assign_background(label_image, bground_image);
    
    // filter small grains
    filter_small_grains(label_image);
    
    // eliminate tile borders
    if (eliminate_borders)
    delete_tile_borders(label_image, tile_borders);

    // create grain label image
    vigra::IImage grain_label_image(mWidth,mHeight);
    nr_regions = vigra::labelImageWithBackground(vigra::srcImageRange(label_image), vigra::destImage(grain_label_image), false, 0);
//    nr_grains = nr_regions;
    std::cout<<"Number of regions: "<<nr_regions<<std::endl;
    
    if (nr_regions == 0)
    {
        std::cout<<"segmentation terminated: the image contains no regions."<<std::endl;
        return 2;
    }
    
    // perform region classifier to distinguish between different types of regions
    std::vector<Segment> Segments(nr_regions+1); //region index starts with 1
    classify_segments(Segments, grain_label_image, bground_image);
    
        
    // calculate grains
    std::vector<Grains> Grain(nr_grains+1);
    calculate_grains(Grain, Segments, vector_image, grain_label_image);
    
    
    
    /*********************************************/
    /* Region growth and extraction of           */
    /* topological network                       */
    /*********************************************/
    
    std::cout<<"region growing...";
    
    // create a statistics functor for region growing
    vigra::ArrayOfRegionStatistics<vigra::SeedRgDirectValueFunctor<float> > gradstat(nr_regions);
    
    // perform region growing    
    vigra::IImage boundary_label_image(mWidth,mHeight);
    vigra::seededRegionGrowing(vigra::srcImageRange(grain_label_image), vigra::srcImage(grain_label_image), vigra::destImage(boundary_label_image),
        gradstat, vigra::CompleteGrow);
    std::cout<<"done"<<std::endl;
/*
    // mask background in boundary label image
    for (int y=0; y<mHeight; y++)
        for (int x=0; x<mWidth; x++)
            if (bground_image(x,y)==0)
                boundary_label_image(x,y) = 0;    
*/

    //visualisation of the regions
    vigra::IImage regions(mWidth,mHeight);
    
    int x_old=0;
    int y_old[mWidth];

    for (int x=0;x<mWidth;x++) y_old[x]=0;
 
    for(int y=0;y<mHeight;y++)
    {
        for(int x=0;x<mWidth;x++)
        {
            if (boundary_label_image(x,y)!=x_old || boundary_label_image(x,y)!=y_old[x] || x==(mWidth-1) || y==(mHeight-1))
                regions(x,y)=0;
            else regions(x,y)=1;
            x_old=boundary_label_image(x,y);
            y_old[x]=boundary_label_image(x,y);
        }
    }

    
    
    //array to store regions
    unsigned int** region_image=new unsigned int* [mHeight];
    for (int i=0; i<mHeight; i++) region_image[i]=new unsigned int[mWidth];

    for(int y=0;y<mHeight;y++)
        for(int x=0;x<mWidth;x++)
            region_image[y][x]=boundary_label_image(x,y);

    //creating the hdf5 file
    std::cout<<"create hdf5 files..."<<std::endl;
    hid_t file_save=H5Fcreate(filepath_h5file.c_str(), H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);

    //dataspace for region_image
    hsize_t dims[3];
    dims[0] = mHeight;
    dims[1] = mWidth;
    dims[2] = 1;
    hid_t dataspace_id = H5Screate_simple(3, dims, NULL);

    //dataset for region_image    
    hid_t dataset_id = H5Dcreate1(file_save, "/ws_image", H5T_NATIVE_UINT, dataspace_id, H5P_DEFAULT);

    //dataspace for one row
    hsize_t row[3];
    row[0] = 1;
    row[1] = mWidth;  
    row[2] = 1;
    hid_t mdataspace_id = H5Screate_simple(3, row, NULL);

    //loop over rows in region_image
    for(int i= 0; i<mHeight; i++)
    {
        // select file hyperslab
        hsize_t start[3];// start of hyperslab
        hsize_t count[3];// block count
       
        count[0]  = 1;
        count[1]  = mWidth;
        count[2]  = 1;

        start[0]  = i;
        start[1]  = 0;
        start[2]  = 0;
       
        H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, start, NULL, count, NULL);
       
        unsigned int *row_values = region_image[i];
        H5Dwrite(dataset_id, H5T_NATIVE_UINT, mdataspace_id, dataspace_id, H5P_DEFAULT, row_values);

        delete row_values;
    }

    H5Sclose(mdataspace_id);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);

    delete region_image;

    //Close the file
    H5Fclose(file_save);

    std::cout<<"H5 file written and closed"<<std::endl;
 
    compute_and_save_cgp_data_structure(filepath_cgp.c_str(),1,0);

    //Create symbolic links         disabled in slim-version
    /*
    std::string image_id_str = sourcepath;
    int lastInstanceOfSlash = image_id_str.find_last_of("/");                           //Get the last slash before the image's id
    image_id_str = image_id_str.substr(lastInstanceOfSlash + 1, image_id_str.length()); //The image number is inbetween
    
    std::string h5file_out = sourcepath + "/cAxes/" + image_id_str + ".bmp.h5";
    std::string objects_out = sourcepath + "/cAxes/" + image_id_str + ".bmp.objects.h5";
    std::string bmp_out = sourcepath + "/cAxes/" + image_id_str + ".bmp";
    
    symlink(filepath_h5file.c_str(), h5file_out.c_str());
    symlink(filepath_objects.c_str(), objects_out.c_str());
    symlink(filepath_regions.c_str(), bmp_out.c_str());
    */
    
    /****************************************************************************/
    /* Create boundary data structure and calculate c-axis statistics           */
    /****************************************************************************/


    std::vector<Boundaries> Boundary;
    calculate_boundaries(Boundary, Grain, Segments, filepath_objects);

    calculate_statistics(grain_label_image, vector_image, Grain, bground_image);

    
    /**************************************/
    /*       Export image files           */
    /**************************************/
    
    std::cout << "Export image files.";
    
    //export grains black&white
    export_grains_bw(grain_label_image, filepath_grains_bw); std::cout << ".";
    
    //export regions
    export_regions(regions, Boundary, filepath_regions); std::cout << ".";
    
    //export vector_image in rgb color-code
    export_vectorimage(vector_image, filepath_vectors_rgb); std::cout << ".";
    
    //export grains in rgb color-code
    export_grains_rgb(grain_label_image, Grain, filepath_grains_rgb); std::cout << ".";
    
    //export grains in trend-white color-code
    export_grains_trendw(grain_label_image, Grain, filepath_grains_trendw); std::cout << ".";
    
    //export boundaries in rgb color-code
    export_boundary_rgb(boundary_label_image, regions, bground_image, Segments, Grain, Boundary, filepath_boundary_rgb); std::cout << ".";
    
    //export boundaries in trend-white color-code
    export_boundary_trendw(boundary_label_image, regions, bground_image, Segments, Grain, Boundary, filepath_boundary_trendw); std::cout << ".";
    
    //export subgrain misorientation topography (testing)
    export_topo_image(topo_image, filepath_topo_image); std::cout << ".";
    
    std::cout << " done" << std::endl;
    
    /**************************************/
    /*       Export binaries              */
    /**************************************/
    
//    export_vectorimage_bin(filepath_vectors_rgb);    //disabled in slim-version
    
    
    /**************************************/
    /*       Write text files             */
    /**************************************/
    
    std::cout << "Write text files.";
    
    write_general_results(filepath_general_txt); std::cout << ".";

    write_grains_txt(Grain, filepath_grains_txt); std::cout << ".";
    
    //2nd version containing even axis values
//    write_grains_txt2(Grain, filepath_grains2_txt); std::cout << "."; // disabled in slim-version

    write_stereo_txt(Grain, filepath_stereo); std::cout << ".";
    
//    write_xstereo_txt(Boundary, filepath_xstereo); std::cout << ".";
    
    write_boundaries_txt(Boundary, filepath_boundaries_txt); std::cout << ".";

    std::cout << " done" << std::endl;
    
    return 0;
}
