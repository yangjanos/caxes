#pragma once
#include "CImg.h"
#include <vigra/random_forest.hxx>
#include <vigra/hdf5impex.hxx>
#include <vigra/random_forest_hdf5_impex.hxx>

//functions
bool equality_function2(vigra::TinyVector<float, 3> point1, vigra::TinyVector<float, 3> point2);

//colour settings
double red2(double value, double scalemax);
double green2(double value, double scalemax);
double blue2(double value, double scalemax);
void export_colour (vigra::FImage grey_image, int width, int height, std::string fpath, double scalemax);
vigra::FImage make_legend(vigra::FImage input, int width, int height, double scalemax);

int misorientation(float plotmax)
{
    //Filepath settings
    std::string new_directory = sourcepath+"/cAxes";

    std::string filepath_info = sourcepath+"/info.txt";
    std::string filepath_binary = sourcepath+"/data.cis";
    std::string filepath_grains_bw = new_directory+"/grains_bw.png";
    std::string filepath_h5file = new_directory+"/regions.h5";
    std::string filepath_grains2_txt = new_directory+"/grains2.txt";
    std::string filepath_coloured_scalarproduct = new_directory+"/scalarproduct_rgb.png";
    std::string filepath_misorientation = new_directory+"/misorientation.bmp";
    std::string filepath_gradient = new_directory+"/gradient.bmp";

    /*************************************************************/
    /* Load required data structures, cAxes has to be run before */
    /*************************************************************/

    //Import vector_image from data.cis
    int import_status;
    import_status = import_data_g50AWI();
    if (import_status != 0)
        import_status = import_data_g50LGGE();
    if (import_status != 0)
    {
        std::cout << "ERROR: no data found in " << sourcepath << std::endl;
        return import_status;
    }

    // Assign tile borders
    int nr_tile_borders = tiles_high*mWidth+tiles_wide*mHeight;
    int tile_borders[nr_tile_borders][2];
    borderpixel = 0;

    for (int xtile=1; xtile<tiles_wide; xtile++)    //assign left tile borders (first column)
    for (int y=1; y<mHeight; y++)
    {
        tile_borders[borderpixel][0]=tile_size*xtile;
        tile_borders[borderpixel][1]=y;
        borderpixel++;
    }
    for (int ytile=1; ytile<tiles_high; ytile++)    //assign upper tile borders (first row)
    for (int x=1; x<mWidth; x++)
    {
        tile_borders[borderpixel][0]=x;
        tile_borders[borderpixel][1]=tile_size*ytile;
        borderpixel++;
    }


    //Load grain bw image
    FILE *info_grains_bw;
    info_grains_bw = fopen(filepath_grains_bw.c_str(), "rb");

    vigra::BasicImage<bool> grains_bw(mWidth, mHeight);

    if(info_grains_bw == NULL)
    {
        std::cout << "No grain bw image found!" << std::endl;
        exit(-1);
    }
    else
    {
        std::cout << "Load grain bw image" << std::endl;
        vigra::ImageImportInfo info_bool_image(filepath_grains_bw.c_str());
        importImage(info_bool_image, destImage(grains_bw));
        fclose(info_grains_bw);
    }

    //Load regions h5 file
    vigra::BasicImage<unsigned int> grain_label_image;

    std::cout<<"Importing results from file: "<<std::endl;
    std::cout<<filepath_h5file<<std::endl;
    
    //Open existing file
    hid_t file_id0 = H5Fopen(filepath_h5file.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    
    //Open an existing dataset
    hid_t dataset_id = H5Dopen(file_id0, "/ws_image", H5P_DEFAULT);
    
    //Get filespace handle
    hid_t filespace = H5Dget_space(dataset_id);
    
    //Get dimensions
    hsize_t dims[3];    
    H5Sget_simple_extent_dims(filespace, dims, NULL);

    mHeight=dims[0];
    mWidth=dims[1];

    grain_label_image.resize(dims[1], dims[0]);

    //Loop over rows in ws_image
    for(int i = 0; i < dims[0]; i++)
    {
        //Datastructure to read in
        unsigned int * row_values = new unsigned int[dims[1]];
        
        //Dataspace for one row    
        hsize_t row[3];
        row[0] = 1;
        row[1] = dims[1];
        row[2] = 1;
        hid_t mdataspace_id = H5Screate_simple(3, row, NULL);

        //Select file hyperslap
        hsize_t start_h[3]; //Start of hyperslab
        hsize_t count[3];   //Block count

        count[0] = 1;
        count[1] = dims[1];
        count[2] = 1;

        start_h[0] = i;
        start_h[1] = 0;
        start_h[2] = 0;
        
        H5Sselect_hyperslab(filespace, H5S_SELECT_SET, start_h, NULL, count, NULL);
        H5Dread(dataset_id, H5T_NATIVE_UINT, mdataspace_id, filespace, H5P_DEFAULT, row_values);

        for(int x = 0; x < dims[1]; x++)
        {
            unsigned int value = row_values[x];
            grain_label_image(x, i) = value;
        }
    
        //Close the memory space
        H5Sclose(mdataspace_id);

        delete row_values;
    }
    
    //Close the file space
    H5Sclose(filespace);

    //Close the data set
    H5Dclose(dataset_id);

    //Close the file
    H5Fclose(file_id0);

    std::cout<<"...done"<<std::endl;

    // set border in grain_label_image to zero
    for (int y = 0; y < mHeight; y++)
        for (int x = 0; x < mWidth; x++)
        {
            if (grains_bw(x,y)==false)
            {
                grain_label_image(x,y) = 0;
            }
        }

    //Load (2nd version of) grains file
    std::vector<Grains> Grain(1);
    nr_grains=0;

    std::ifstream grains_file(filepath_grains2_txt.c_str());

    //string is read from temp file to check whether file is empty
    std::string teststring;
    std::ifstream temp_grains_file(filepath_grains2_txt.c_str());
    temp_grains_file>>teststring;

    if(grains_file && teststring.size()!=0)
    {
        int line_counter = 0;
        bool line_found=false;//find first line
        while(!grains_file.eof() && !line_found)
        {
            line_counter++;
            char line[512];
            grains_file.getline(line,512);
            if(line_counter == 20)
            {
              std::string rotangleline = line;
              std::istringstream(std::string (rotangleline.begin()+33, rotangleline.end())) >> rot_angle;
            }
            if(!std::strcmp(line, "# c10 regelungsgrad [%]")) line_found=true;
        }
        
        std::cout << "rot_angle = " << rot_angle << std::endl;

        while(!grains_file.eof())
        {
            int nr=0;
            Grains entry;

            grains_file>>nr>>entry.size>>entry.xcenter>>entry.ycenter>>entry.axis[0]>>entry.axis[1]>>entry.axis[2]>>entry.az>>entry.co>>entry.r;

            if(nr>0)
            {
                Grain.push_back(entry);
                nr_grains++;
            }
        }

        grains_file.close();
        temp_grains_file.close();

        std::cout << "Loaded data of "<<nr_grains<<" grains"<<std::endl;
    }
    else
    {
        std::cout << "Error reading "<<filepath_grains2_txt<<"!" << std::endl;
        exit(-1);
    }
    
    //Rotate tiles in vector_image
    if (rot_angle != 0.0)
    {
        rotate_vector_image(vector_image, tile_borders);
    }


    /****************************************************/
    /* calculating relative misorientation within grain */
    /****************************************************/

    //calculate misorientation relative to grain mean value 
    vigra::BasicImage< vigra::TinyVector <float, 3> > misorient_image(mWidth, mHeight);

    for (int y=0; y<mHeight; y++)
    {
        for (int x=0; x<mWidth; x++)
        {
            if (vector_image(x,y)[2]<0.0) vector_image(x,y)=-vector_image(x,y);
            int grain = grain_label_image(x,y);
            vigra::TinyVector<float, 3> offset;
            offset[0]=offset[1]=offset[2]=0.2;
            misorient_image(x,y)=vector_image(x,y)-Grain[grain].axis+offset;
            misorient_image(x,y)[0]=fabs(misorient_image(x,y)[0]);
            misorient_image(x,y)[1]=fabs(misorient_image(x,y)[1]);
            misorient_image(x,y)[2]=fabs(misorient_image(x,y)[2]);
            if (grain_label_image(x,y)==0) misorient_image(x,y)[0]=misorient_image(x,y)[1]=misorient_image(x,y)[2]=0.0;
        }
    }

    //exportImage(srcImageRange(misorient_image), vigra::ImageExportInfo(filepath_misorient.c_str())); 


    /*****************************************************************/
    /*calculate scalarproduct and angle relative to grain mean value */
    /*****************************************************************/

    /*Here for each pixel the angle of the c-axis relative to the grain mean value is calculated.
      A scalar map (called scalar_product_image) of this misorientation value is produced.
      The maximum value of the map (in rad) can be set as plotmax1.
      The grain boundaries from grain_label_image are copied into the map.*/

    vigra::FImage scalarproduct_image(mWidth, mHeight);
    vigra::FImage scalarproduct_transformed(mWidth, mHeight);
    double legendmax1=0;// a counter that takes the value of the maximum misorientation of the sample
    double plotmax1=plotmax;// adjustable maximum value of the misorientation map

    for (int grain=1; grain<=nr_grains; grain++)//normalisation of the grain mean c-axis
    {
        double axis_norm = sqrt(vigra::squaredNorm(Grain[grain].axis));
        Grain[grain].axis=Grain[grain].axis/axis_norm;
    }

    for (int y=0; y<mHeight; y++)
    {
        for (int x=0; x<mWidth; x++)
        {
            if (vector_image(x,y)[2]<0.0) vector_image(x,y)=-vector_image(x,y);
            int grain = grain_label_image(x,y);
            scalarproduct_image(x,y)=vector_image(x,y)[0]*Grain[grain].axis[0]+vector_image(x,y)[1]*Grain[grain].axis[1]+vector_image(x,y)[2]*Grain[grain].axis[2];
            if (scalarproduct_image(x,y)< 0.0) scalarproduct_image(x,y)=-scalarproduct_image(x,y);
            if (scalarproduct_image(x,y)>=1.0) scalarproduct_image(x,y)=1.0;
            scalarproduct_image(x,y)=acos(scalarproduct_image(x,y))*180/M_PI;
            if (grain_label_image(x,y)==0) scalarproduct_image(x,y)=0.0;
            //if (grain_label_image(x,y)!=389) scalarproduct_image(x,y)=0.0; //a single grain can be chosen, all other grains will appear black
            if (scalarproduct_image(x,y) > 5.729577951f) scalarproduct_image(x,y)=5.729577951f;
        }
    }

    vigra::FindMinMax<vigra::FImage::PixelType> minmax;
    vigra::inspectImage(srcImageRange(scalarproduct_image), minmax);

    vigra::transformImage(srcImageRange(scalarproduct_image), destImage(scalarproduct_transformed),
        vigra::linearIntensityTransform(255.0 / (minmax.max - minmax.min), - minmax.min)); 

    std::vector<vigra::FImage> vector_of_images;
    vigra::FImage temp_image(mWidth,mHeight), temp_image2(mWidth,mHeight);

    vigra::FImage stxx(mWidth,mHeight), stxy(mWidth,mHeight), styy(mWidth,mHeight);

    std::cout<<"Calculate Structure Tensor"<<std::endl;
    structureTensor(srcImageRange(scalarproduct_transformed),destImage(stxx), destImage(stxy), destImage(styy), 1.5f, 0.9f);

    #pragma omp parallel
    {
        #pragma omp for
        for(int y=0;y<mHeight;y++)
        {
            for(int x=0;x<mWidth;x++)
            {
                float lambda1;
                float lambda2;
                float d= ((stxx(x,y)-styy(x,y))/2);
                lambda1= (((stxx(x,y)+styy(x,y))/2) + sqrt(d*d+ stxy(x,y)*stxy(x,y))) ;
                lambda2= (((stxx(x,y)+styy(x,y))/2) - sqrt(d*d+ stxy(x,y)*stxy(x,y))) ;

                temp_image(x,y)=lambda1;
                temp_image2(x,y)=lambda2;
            }
        }
    }

    vector_of_images.push_back(temp_image);
    vector_of_images.push_back(temp_image2);

    std::cout<<"Calculate Structure Tensor"<<std::endl;
    structureTensor(srcImageRange(scalarproduct_transformed),destImage(stxx), destImage(stxy), destImage(styy), 2.0f, 1.2f);

    #pragma omp parallel
    {
        #pragma omp for
        for(int y=0;y<mHeight;y++)
        {
            for(int x=0;x<mWidth;x++)
            {
                float lambda1;
                float lambda2;
                float d= ((stxx(x,y)-styy(x,y))/2);
                lambda1= (((stxx(x,y)+styy(x,y))/2) + sqrt(d*d+ stxy(x,y)*stxy(x,y))) ;
                lambda2= (((stxx(x,y)+styy(x,y))/2) - sqrt(d*d+ stxy(x,y)*stxy(x,y))) ;

                temp_image(x,y)=lambda1;
                temp_image2(x,y)=lambda2;
            }
        }
    }

    vector_of_images.push_back(temp_image);
    vector_of_images.push_back(temp_image2);

    std::cout<<"Calculate Hessian Matrix of Gaussian"<<std::endl;
    hessianMatrixOfGaussian(srcImageRange(scalarproduct_transformed),destImage(stxx), destImage(stxy), destImage(styy), 1.0f);

    #pragma omp parallel
    {
        #pragma omp for
        for(int y=0;y<mHeight;y++)
        {
            for(int x=0;x<mWidth;x++)
            {
                float lambda1;
                float d=((stxx(x,y)-styy(x,y))/2);
                lambda1= (  ((stxx(x,y)+styy(x,y))/2) + sqrt(d*d+ stxy(x,y)*stxy(x,y))  ) ;

                temp_image(x,y)=lambda1;
            }
        }
    }

    vector_of_images.push_back(temp_image);

    std::cout<<"Calculate Hessian Matrix of Gaussian"<<std::endl;
    hessianMatrixOfGaussian(srcImageRange(scalarproduct_transformed),destImage(stxx), destImage(stxy), destImage(styy), 1.5f);

    #pragma omp parallel
    {
        #pragma omp for
        for(int y=0;y<mHeight;y++)
        {
            for(int x=0;x<mWidth;x++)
            {
                float lambda1;
                float lambda2;
                float d=((stxx(x,y)-styy(x,y))/2);
                lambda2= (  ((stxx(x,y)+styy(x,y))/2) - sqrt(d*d+ stxy(x,y)*stxy(x,y))  ) ;

                temp_image(x,y)=lambda2;
            }
        }
    }

    vector_of_images.push_back(temp_image);

    std::string filepath_to_random_forest_file="RF4b_rf.hdf5";

    vigra::RandomForest<> rf;

    rf_import_HDF5(rf,filepath_to_random_forest_file.c_str());

    std::cout<<"Probability prediction..."<<std::endl;

    vigra::FImage probability_image(mWidth,mHeight);

    #pragma omp parallel
    {
        float * pixel_unknown_features_array=new float[vector_of_images.size()];
        vigra::MultiArray<2, float> pixel_unknown_features(vigra::MultiArrayShape<2>::type(1, vector_of_images.size()),
            pixel_unknown_features_array);

        #pragma omp for
        for(int x=0;x<mWidth;x++)
        {
            for (int y=0;y<mHeight;y++)
            {
                for(int feature=0;feature<vector_of_images.size();feature++)
                {
                    pixel_unknown_features(0,feature)=vector_of_images[feature](x,y);
                }

                vigra::MultiArray<2, double> pixel_unknown_probability(vigra::MultiArrayShape<2>::type(1,2));

                rf.predictProbabilities(pixel_unknown_features,pixel_unknown_probability);

                probability_image(x,y)=(float)pixel_unknown_probability(0,0);
            }
        }

        delete pixel_unknown_features_array;
    }

    std::cout<<"...done"<<std::endl;

    exportImage(srcImageRange(probability_image), vigra::ImageExportInfo(filepath_misorientation.c_str()));

    for (int y=0; y<mHeight; y++)
    {
        for (int x=0; x<mWidth; x++)
        {
            if (scalarproduct_image(x,y) > legendmax1) legendmax1=scalarproduct_image(x,y);
            if (scalarproduct_image(x,y) > plotmax1) scalarproduct_image(x,y)=plotmax1;
        }
    }

    scalarproduct_image=make_legend(scalarproduct_image, mWidth, mHeight, plotmax1);//a colour legend is produced in the right bottom corner of the map
    export_colour (scalarproduct_image, mWidth, mHeight, filepath_coloured_scalarproduct, plotmax1);
    /*
    // calculate gradient-magnitude
    vigra::FImage gradient_image(mWidth, mHeight);
    vigra::gaussianGradientMagnitude(srcImageRange(scalarproduct_image), destImage(gradient_image), 1.0);

    exportImage(srcImageRange(gradient_image), vigra::ImageExportInfo(filepath_gradient.c_str()));
    */
    /*************************************************/
    /*calculation and plotting of standard deviation */
    /*************************************************/

    /*double plotmax3=0.1;
    double devmax=0.0;
    for(int grain=1; grain<=nr_grains; grain++)
    {
        for (int y=0; y<mHeight; y++)
        {
            for (int x=0; x<mWidth; x++)
            {
                if (grain_label_image(x,y)==grain)
                {
                    Grain[grain].standarddev=Grain[grain].standarddev+scalarproduct_image(x,y)*scalarproduct_image(x,y);
                }
            }
        }
        Grain[grain].standarddev=sqrt(Grain[grain].standarddev/Grain[grain].size);
        if (devmax<=Grain[grain].standarddev) devmax=Grain[grain].standarddev;
    }
    std::cout << devmax << std::endl;

    vigra::FImage standarddev_image(mWidth, mHeight);
    for (int y=0; y<mHeight; y++)
    {
        for (int x=0; x<mWidth; x++)
        {
            standarddev_image(x,y)=Grain[grain_label_image(x,y)].standarddev;
            if (grain_label_image(x,y)==0) standarddev_image(x,y)=0.0;
            if (standarddev_image(x,y) >= plotmax3) standarddev_image(x,y)=plotmax3;
        }
    }
    standarddev_image=make_legend(standarddev_image, mWidth, mHeight, plotmax3);*/

    //exportImage(srcImageRange(standarddev_image), vigra::ImageExportInfo(filepath_standarddev.c_str()));
    //export_colour (standarddev_image, mWidth, mHeight, filepath_coloured_standarddev, plotmax3);


    /************************************************************/
    /*calculate misorientation relative to grain centre of mass */
    /************************************************************/

    /*vigra::FImage centerorientation_image(mWidth, mHeight);

    for (int y=0; y<mHeight; y++)
    {
        for (int x=0; x<mWidth; x++)
        {
            if (vector_image(x,y)[2]<0.0) vector_image(x,y)=-vector_image(x,y);
            int grain = grain_label_image(x,y);
            //centerorientation_image(x,y)=vigra::dot(vector_image(x,y),vector_image(Grain[grain].xcenter,Grain[grain].ycenter));
            centerorientation_image(x,y)=vector_image(x,y)[0]*vector_image(Grain[grain].xcenter,Grain[grain].ycenter)[0]+
                vector_image(x,y)[1]*vector_image(Grain[grain].xcenter,Grain[grain].ycenter)[1]+
                vector_image(x,y)[2]*vector_image(Grain[grain].xcenter,Grain[grain].ycenter)[2];
            if (centerorientation_image(x,y)<0.0) centerorientation_image(x,y)=-centerorientation_image(x,y);
            centerorientation_image(x,y)=acos(centerorientation_image(x,y));
            if (grain_label_image(x,y)==0) centerorientation_image(x,y)=0.0;
        }
    }*/
   

    /**********************************************************************************/
    /*calculate misorientation relative to a reference pixel on the edge of the grain */
    /**********************************************************************************/

    /*vigra::FImage edgereference_image(mWidth, mHeight);
    double legendmax2=0;
    double plotmax2=0.2;

    for (int grain=1; grain<=nr_grains; grain++)
    {
        for (int y=0; y<mHeight; y++)
        {
            int x;
            for (x=0; x<mWidth; x++)
            {
                if (grain_label_image(x,y)==grain)
                {
                    Grain[grain].edgeref=vector_image(x,y);
                    break;
                }
            }
            if (grain_label_image(x,y)==grain) break;
        }
    }


    for (int y=0; y<mHeight; y++)
    {
        for (int x=0; x<mWidth; x++)
        {
            if (vector_image(x,y)[2]<0.0) vector_image(x,y)=-vector_image(x,y);
            int grain = grain_label_image(x,y);
            edgereference_image(x,y)=fabs(vigra::dot(vector_image(x,y), Grain[grain].edgeref));
            edgereference_image(x,y)=acos(edgereference_image(x,y));
            if (grain_label_image(x,y)==0) edgereference_image(x,y)=0.0;
            if (edgereference_image(x,y) > legendmax2) legendmax2=edgereference_image(x,y);
            if (edgereference_image(x,y) > plotmax2) edgereference_image(x,y)=plotmax2;
        }
    }

    std::cout << legendmax2 << std::endl;
    edgereference_image=make_legend(edgereference_image, mWidth, mHeight, plotmax2);*/

   //exportImage(srcImageRange(edgereference_image), vigra::ImageExportInfo(filepath_edgereference.c_str()));
   //export_colour (edgereference_image, mWidth, mHeight, filepath_coloured_edgereference, plotmax2);


    /*******************************************************************************/
    /*       calculating misorientation distribution per grain as a histogram      */
    /*******************************************************************************/

    /*const int bin_nr=100;
    double histogram[bin_nr][3];
    for (int i=0; i<bin_nr; i++)
    for (int j=0; j<3; j++)
        histogram[i][j]=0;
    for (int i=0; i<bin_nr; i++)
    {
        histogram[i][0]=(i+0.5)*plotmax1/(double)bin_nr;
        histogram[i][1]=histogram[i][0]*180/M_PI;
    }

    for (int y=0; y<mHeight; y++)//ignore the outer zones for a total view histogram!
    {
        for (int x=0; x<mWidth; x++)
        {
            if (grain_label_image(x,y)==389)
            {
                for (int i=1; i<bin_nr-1; i++)
                {
                    if (scalarproduct_image(x,y)>histogram[i][0] && scalarproduct_image(x,y)<=histogram[i+1][0])
                    {
                        histogram[i][2]++;
                        break;
                    }
                }
                if (scalarproduct_image(x,y)>histogram[bin_nr-1][0]) histogram[bin_nr-1][2]++; 
            }
            }
    }

    std::ofstream histo_file(filepath_histogram.c_str());
    histo_file <<"#misorientation[rad]" << "\t" << "misorientation[deg]" << "\t" << "count" << "\n";
    for (int i=0; i< bin_nr; i++)
        histo_file << histogram[i][0] << "\t" << histogram[i][1] << "\t" << histogram[i][2] << "\n";
    histo_file.close();
    */

    return 0;
}

bool equality_function2(vigra::TinyVector<float, 3> point1, vigra::TinyVector<float, 3> point2)
{   
    float misorientation = acos(fabs(vigra::dot(point1, point2)));
    return 0.2*M_PI/180 >= misorientation;
}

double blue2(double value, double scalemax)
{
    if (value<=scalemax/4) return scalemax;
    if (value>scalemax/4 && value<=2*scalemax/4) return 2*scalemax-4*value;
    else return 0;
}
 
double red2(double value, double scalemax)
{
    if (value<=scalemax/2) return 0;
    if (value>scalemax/2 && value<=3*scalemax/4) return 4*value-4*scalemax/2;
    else return scalemax;
}

double green2(double value, double scalemax)
{
    if (value<=scalemax/4) return 4*value;
    if (value>scalemax/4 && value<=3*scalemax/4) return scalemax;
    if (value>3*scalemax/4 && value<=scalemax) return 4*scalemax-4*value;
    else return 0;
}

void export_colour(vigra::FImage grey_image, int width, int height, std::string fpath, double scalemax)
{
   vigra::FRGBImage output_image(width,height);
   for(int y=0;y<height;y++)
   {
        for(int x=0;x<width;x++)
        {
            output_image(x,y)[0]=red2(grey_image(x,y), scalemax);
            output_image(x,y)[1]=green2(grey_image(x,y), scalemax);
            output_image(x,y)[2]=blue2(grey_image(x,y), scalemax);
        if (grey_image(x,y)==0.0) output_image(x,y)[0]=output_image(x,y)[1]=output_image(x,y)[2]=0;//black
        }
   }

   exportImage(srcImageRange(output_image), vigra::ImageExportInfo(fpath.c_str()));
}

vigra::FImage make_legend(vigra::FImage input, int width, int height, double scalemax)
{
    for (int y=(height-100); y<height; y++)
    {
        int x=0;
        while (x<255)
        {
            for (int place=width-1024+4*x-3; place<=width-1024+4*x; place++)
            {
                input(place,y)=x*scalemax/255;
            }
            x=x+1;
        }
    }

    cimg_library::CImg<double> temp(width, height, 1, 1);
    for(int y=(height-210);y<height;y++)
        for(int x=(width-1024);x<width;x++)
            temp(x,y,0,0)=input(x,y);
    std::string pmax;
    std::stringstream convert;
    convert << scalemax;
    pmax=convert.str();
    double *foreground_colour= new double;
    *foreground_colour=0.0;
    double *text_colour= new double;
    *text_colour=scalemax;
    double *background_colour= NULL;
    temp.draw_text((width-80), (height-100), pmax.c_str(),foreground_colour, background_colour, 1.0,100);
    temp.draw_text((width-1020), (height-100), "0.0",foreground_colour, background_colour, 1.0,100);
    temp.draw_text((width-1020), (height-150), "misorientation[deg]",text_colour, background_colour, 1.0,100);
    for(int y=(height-210);y<height;y++)
        for(int x=(width-1024);x<width;x++)
            input(x,y)=temp(x,y,0,0);
    return input;
}
